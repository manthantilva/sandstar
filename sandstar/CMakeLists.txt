cmake_minimum_required(VERSION 3.2.0)
project(sandstar VERSION 0.0.1)
set(CMAKE_CXX_STANDARD 11)

unset(CMAKE_INSTALL_PREFIX)
if(${CMAKE_INSTALL_PREFIX} STREQUAL "/usr/local")
set(CMAKE_INSTALL_PREFIX "/home/eacio/sandstar/")
endif(${CMAKE_INSTALL_PREFIX} STREQUAL "/usr/local")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

##Setup Variables
include(SetUpVariables)
##Setup Poco build
include(SetUpPoco)

add_subdirectory(engine)
add_subdirectory(EacIo)

##Setup Package
include(SetUpPackage)
