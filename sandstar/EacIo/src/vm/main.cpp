
#include "Poco/DateTimeFormatter.h"
#include "Poco/Task.h"
#include "Poco/TaskManager.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include <csignal>
#include <cstdio>
#include <string.h>
#include <iostream>
#include <thread>

#include <sys/stat.h>

#include "errorcodes.h"
#include "sedona.h"

#include "haystack.hpp"
#include "logger.h"

#define STRX(x) #x
#define STR(x) STRX(x)

extern NativeMethod *nativeTable[];
extern "C" void sys_Sys_sleep(SedonaVM *vm, Cell *params);

static int g_terminate = 0;
int willTerminate() { return g_terminate; }
extern "C" int willTerminate_c() { return willTerminate(); }
static void terminate(int sig) {
  // LOG_CRIT_STARTUP_MSG("\n-- Terminate --\n\n");
  g_terminate = 1;
}

extern "C" int engineio_main();
extern "C" int engineio_init();
extern "C" int engineio_pre_init();
extern "C" int engineio_exit();
extern "C" int engineio_stop();
void *engineio_main_wrap(void *arg) { return (void *)(engineio_main()); }
static void onAssertFailure(const char *location, uint16_t linenum) {
  std::cout << "ASSERT FAILURE: " << location << " [Line " << linenum << "]"
            << std::endl;
}
class SampleTask : public Poco::Task {
public:
  SampleTask() : Poco::Task("SampleTask") {}

  void runTask() {
    Poco::Util::Application &app = Poco::Util::Application::instance();
    while (!isCancelled()) {
      Poco::Util::Application::instance().logger().information(
          "busy doing nothing... " +
          Poco::DateTimeFormatter::format(app.uptime()));
      Poco::Task::sleep(1000);
    }
  }
  void cancel() override {
    // engineio_stop();
    Task::cancel();
  }
};
class EngineTask : public Poco::Task {
public:
  EngineTask() : Poco::Task("EngineTask") {}

  void runTask() { engineio_main_wrap(nullptr); }
  void cancel() override {
    engineio_stop();
    Task::cancel();
  }
};
class SedonaTask : public Poco::Task {
public:
  SedonaTask() : Poco::Task("SedonaTask") {}

  void runTask() {
    // engineio_main_wrap();
  }
  void setPlatFile(const std::string &file) { m_strPlatFile = file; }

private:
  std::string m_strPlatFile;
};
class SampleServer : public Poco::Util::ServerApplication {
public:
  SampleServer() : m_bRunLogic(true) {
    ptrHttpServerParams = new Poco::Net::HTTPServerParams();
    std::signal(SIGINT, &SampleServer::signalHandler);
    std::signal(SIGTERM, &SampleServer::signalHandler);
  }

  ~SampleServer() {}
  static void signalHandler(int sig) {
    m_intStop = 1;
  }

protected:
  void initialize(Poco::Util::Application &self) {
    loadConfiguration();
    loadConfiguration(config().getString("application.dir") + "../etc/" +
                      config().getString("application.baseName") +
                      ".properties");
    Poco::Util::ServerApplication::initialize(self);
    haystack::Logger::LogLevel logLevel = haystack::Logger::levelStringToClass(
        config().getString("level", "DEBUG"));
    haystack::Logger::enableLogFilter(
        haystack::Logger::filterStringToClass("ALL"), logLevel);
    haystack::Logger::setLoggerType(haystack::Logger::LoggerType::STDERR_ONLY);

    config().setString("application.configDir",
                       config().getString("application.dir") + "../etc/");
    config().setString("application.dataDir",
                       config().getString("application.dir") + "../data/");
    m_strKitsScode = config().getString("application.dataDir") + "kits.scode";
    m_strAppSab = config().getString("application.dataDir") + "app.sab";

    logger().information("starting up");
    LOG_DEBUG_STARTUP_MSG("starting up");
  }

  void uninitialize() {
    logger().information("shutting down");
    LOG_DEBUG_STARTUP_MSG("shutting up");
    Poco::Util::ServerApplication::uninitialize();
  }

  void defineOptions(Poco::Util::OptionSet &options) {
    Poco::Util::ServerApplication::defineOptions(options);

    options.addOption(
        Poco::Util::Option("help", "h",
                           "display help information on command line arguments")
            .required(false)
            .repeatable(false)
            .callback(Poco::Util::OptionCallback<SampleServer>(
                this, &SampleServer::handleHelp)));
    options.addOption(
        Poco::Util::Option("version", "v", "display version information")
            .required(false)
            .repeatable(false)
            .callback(Poco::Util::OptionCallback<SampleServer>(
                this, &SampleServer::handleVersion)));
    options.addOption(Option("plat", "p", "load plat file")
                          .required(false)
                          .repeatable(false)
                          .callback(Poco::Util::OptionCallback<SampleServer>(
                              this, &SampleServer::handlePlat)));
  }

  void handleHelp(const std::string &name, const std::string &value) {
    m_bRunLogic = false;
    displayHelp();
    stopOptionsProcessing();
  }
  void handlePlat(const std::string &name, const std::string &value) {
    m_bPlatformMode = true;
  }
  void handleVersion(const std::string &name, const std::string &value) {
    m_bRunLogic = false;

#ifdef __QNX__
                std::cout<<"Sedona VM"<< STR(PLAT_BUILD_VERSION))<<std::endl;
#else // __QNX__
    std::cout << "Sedona VM" << PLAT_BUILD_VERSION << std::endl;
#endif // __QNX__
                std::cout << "buildDate:" << __DATE__ << " " << __TIME__
                          << std::endl;
#ifdef IS_BIG_ENDIAN
                std::cout << "endian:    big" << std::endl;
#else // IS_BIG_ENDIAN
                std::cout << "endian:    little" << std::endl;
#endif // IS_BIG_ENDIAN

                std::cout << "blockSize: " << SCODE_BLOCK_SIZE << std::endl;
                std::cout << "refSize:   " << sizeof(void *) << std::endl;
                std::cout << std::endl;
                stopOptionsProcessing();
  }

  void displayHelp() {
    Poco::Util::HelpFormatter helpFormatter(options());
    helpFormatter.setCommand(commandName());
    helpFormatter.setUsage("OPTIONS");
    helpFormatter.setHeader("A sample server application that demonstrates "
                            "some of the features of the "
                            "Util::ServerApplication class.");
    helpFormatter.format(std::cout);
  }

  int main(const ArgVec &args) {
    if (m_bRunLogic) {
      int ret = 0;
      if (config().getBool("engine", true)) {
		engineio_pre_init();
        ret = system(std::string(config().getString("application.dir") +
                                 "engine" + STR(EXECUTABLE_POSTFIX) + " start")
                         .c_str());
        engineio_init();
        Poco::Thread::sleep(1000);
      } else {
        LOG_INFO_STARTUP_MSG("Engine is disabled");
      }
      if (config().getBool("haystack", true)) {
#if LOCALHOST_ENABLE
        Poco::Net::ServerSocket svs(Poco::Net::SocketAddress(
            config().getString("listen", "127.0.0.1"),
            static_cast<unsigned short>(
                config().getInt("haystack.port", 8085))));
#else  // LOCALHOST_ENABLE
        Poco::Net::ServerSocket svs(static_cast<unsigned short>(
            config().getInt("haystack.port", 8085)));
#endif // LOCALHOST_ENABLE

        ptrHttpServerParams->setMaxQueued(
            config().getInt("haystack.maxQueued", 100));
        ptrHttpServerParams->setMaxThreads(
            config().getInt("haystack.maxThreads", 16));
        ThreadPool::defaultPool().addCapacity(config().getInt("haystack.maxThreads", 16));
        ptrHttpServerParams->setKeepAlive(true);
        haystack::PointServer* m_pPointServer = new haystack::PointServer();

        m_ptrHttpServer =
            std::unique_ptr<Poco::Net::HTTPServer>(new Poco::Net::HTTPServer(
                new HaystackRequestHandlerFactory(*m_pPointServer), svs,
                ptrHttpServerParams));
        m_ptrHttpServer->setConnectionFilter(new RejectFilter);
        m_ptrHttpServer->start();
        Poco::Thread::sleep(1000);
      } else {
        LOG_INFO_STARTUP_MSG("Haystack server is disabled");
      }
      if (config().getBool("engine", true)) {
        m_oEngineThread = startEngine();
        Poco::Thread::sleep(1000);
      }
      if (config().getBool("sedona", true)) {
        if (m_bPlatformMode) {
          std::cout << "Platform mode" << std::endl;
          m_oSedonaThread = startSedona();
          Poco::Thread::sleep(1000);
        } else {
          std::cout << "StandAlone Mode Not Implemented Yet" << std::endl;
        }
      } else {
        LOG_INFO_STARTUP_MSG("SedonaVM is disabled");
      }
      while (!m_intStop) {
        Poco::Thread::sleep(500);
      }
      if (config().getBool("sedona", true)) {
        if (m_bPlatformMode) {
          stopSedona(m_oSedonaThread);
          // m_oSedonaThread = startSedona();
        }
      }
      if (config().getBool("haystack", true)) {
        m_ptrHttpServer->stop();
      }
      if (config().getBool("engine", true)) {
        ret = system(std::string(config().getString("application.dir") +
                                 "engine" + STR(EXECUTABLE_POSTFIX) + " stop")
                         .c_str());
        stopEngine(m_oEngineThread);
		engineio_pre_init();
      }
    }
    return Poco::Util::Application::EXIT_OK;
  }

private:
  std::thread startEngine() {
    std::thread oEngine{engineio_main_wrap, nullptr};
    return std::move(oEngine);
  }
  std::thread startSedona() {
    std::thread oSedona{&SampleServer::runInPlatformMode,std::ref(*this)};
    return std::move(oSedona);
  }
  bool isFileExist(const std::string &file) {
    struct stat buffer;
    return (stat(file.c_str(), &buffer) == 0);
  }
  void checkStaged(const std::string &file) {
    if (isFileExist(file + ".stage")) {
      std::remove(file.c_str());
      std::rename((file + ".stage").c_str(), file.c_str());
    }
  }
  int runInPlatformMode() {
    int result = 0;
    SedonaVM vm;
    bool quit = false;
    do {
      checkStaged(m_strKitsScode.c_str());
      checkStaged(m_strAppSab.c_str());
      if ((result = commonVmSetup(&vm, m_strKitsScode.c_str())) != 0) {
        return result;
      }
      // const char *app = m_strAppSab.data();
      const char* app = strdup(m_strAppSab.c_str());
      // std::strcpy(app,m_strAppSab.c_str());
      // app[m_strAppSab.size()] = '\0';
      vm.args = (const char **)&app;
      vm.argsLen = 1;

    std::cout<<__FILE__<<":"<<__LINE__<<std::endl;
      result = vmRun(&vm);
    std::cout<<__FILE__<<":"<<__LINE__<<std::endl;
      if (result == ERR_STOP_BY_USER) {
        LOG_INFO_STARTUP_MSG("Stopped on user request");
        break;
      }
      while ((result == ERR_HIBERNATE) || (result == ERR_YIELD)) {
        if (m_intStop) {
          return 0;
        }

        //  Simulate hibernate and yield
        if (result == ERR_HIBERNATE) {
          LOG_INFO_STARTUP_MSG("-- Simulated hibernate --");
        } else {
          sys_Sys_sleep(NULL, (Cell *)&yieldNs);
          yieldNs = 0;
        }

        result = vmResume(&vm);
      }
      if (result != 0) {
        if (result == ERR_RESTART) {
          LOG_ERR_STARTUP_MSG("Restarting VM\n\n");
        } else {
          LOG_INFO_STARTUP_MSG("Cannot run VM (%d)\n", result);
          quit = true;
        }
      } else {
        LOG_INFO_STARTUP_MSG("Quitting\n");
        quit = true;
      }
    } while (!quit);
    return result;
  }
  int commonVmSetup(SedonaVM *vm, const char *scodeFile) {
    int result;

    // load scode
    result =
        loadFile(scodeFile, (uint8_t **)&(vm->codeBaseAddr), &(vm->codeSize));
    if (result != 0) {
      LOG_ERR_STARTUP_MSG("Cannot load input file (%d): %s\n", result,
                          scodeFile);
      return result;
    }

    // alloc stack (hardcoded for now)
    vm->stackMaxSize = 16384;
    vm->stackBaseAddr = (uint8_t *)malloc(vm->stackMaxSize);
    if (vm->stackBaseAddr == NULL) {
      LOG_ERR_STARTUP_MSG("Cannot malloc stack segments\n");
      return ERR_MALLOC_STACK;
    }

    // setup callbacks
    vm->onAssertFailure = onAssertFailure;

    // setup native method table
    vm->nativeTable = nativeTable;

    // setup call function pointer
    vm->call = vmCall;
    return 0;
  }
  int loadFile(const char *filename, uint8_t **paddr, size_t *psize) {
    size_t result;
    FILE *file;
    size_t size;
    uint8_t *addr;

    // open file
    file = fopen(filename, "rb");
    if (file == NULL)
      return ERR_INPUT_FILE_NOT_FOUND;

    // seek to end to get file size
    result = fseek(file, 0, SEEK_END);
    if (result != 0)
      return ERR_CANNOT_READ_INPUT_FILE;
    size = ftell(file);
    rewind(file);

    // allocate memory for image
    addr = (uint8_t *)malloc(size);
    if (addr == NULL)
      return ERR_MALLOC_IMAGE;

    // read file into memory
    result = fread(addr, 1, size, file);
    if (result != size)
      return ERR_CANNOT_READ_INPUT_FILE;

    // success
    *paddr = addr;
    *psize = size;
    fclose(file);
    return 0;
  }
  void stopEngine(std::thread &thread) {
    engineio_stop();
    thread.join();
  }
  void stopSedona(std::thread &thread) {
    // engineio_stop();
    stopVm();
    thread.join();
  }

private:
  bool m_bRunLogic{true};
  bool m_bPlatformMode{false};
  std::unique_ptr<Poco::Net::HTTPServer> m_ptrHttpServer{nullptr};
  Poco::Net::HTTPServerParams::Ptr ptrHttpServerParams;
  std::string m_strPlat;
  static int m_intStop;
  int64_t yieldNs{0};
  std::string m_strKitsScode;
  std::string m_strAppSab;
  std::thread m_oEngineThread;
  std::thread m_oSedonaThread;
};
int SampleServer::m_intStop = 0;

POCO_SERVER_MAIN(SampleServer)
