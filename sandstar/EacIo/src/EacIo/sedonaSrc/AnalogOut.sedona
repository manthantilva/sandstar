//
// File: BoolOut.sedona
//
// Description:
//  
// History:
//   24 Oct 2017  Kushal Dalsania    Baseline
//

**
** analogOutPoint
**   Defines native functions to interact with digital outputs
**

class analogOutPoint
{
  static native bool set(int channel, float value);
}

**
**<b>Analog Output Component.</b>
**
**The Analog Output (AO) component provides an interface to the physical analog output point. There are four analogue output points on EasyIO30P controller that support voltage and current.
**
@niagaraIcon="module://icons/x16/control/numericPoint.png"

class AnalogOutput extends Component
{
  // ------Properties------

  ** The analog output channel name.
  @readonly @asStr property Buf(50) channelName = ""
  
  ** The digital output channel.
  @readonly property int channel;
  
  **current channel voltage
  @readonly property float out = null;
  
  **Analog output priority control
  property float in1 = null;
  @summary=false property float in2 = null;
  @summary=false property float in3 = null;
  @summary=false property float in4 = null;
  @summary=false property float in5 = null;
  @summary=false property float in6 = null;
  @summary=false property float in7 = null;
  property float in8 = null;
  @summary=false property float in9 = null;
  property float in10 = null;
  @summary=false property float in11 = null;
  @summary=false property float in12 = null;
  @summary=false property float in13 = null;
  @summary=false property float in14 = null;
  @summary=false property float in15 = null;
  property float in16 = null;
  property float relinquishDefault = 0.0f;
  
  ** Marker List
  @config @asStr property Buf(100) pointQuery;
  
  ** Count.
  @readonly property int pointQuerySize;
  
  ** point query status. 
  @allowNull=false
  @trueText="ok" @falseText="fault"
  @readonly property bool pointQueryStatus;
  
  ** Temporary Marker List
  inline Str(100) tempPointQuery = "nan";
  
  ** Current Status on channel
  @readonly @asStr property Buf(20) curStatus = "na";
  
  ** Operating Status
  @allowNull=false
  @trueText="yes" @falseText="no"
  @readonly property bool enabled;
  
  ////////////////////////////////////////////////////////////////
  // Class Global
  ////////////////////////////////////////////////////////////////
  private long ticks;
  
  ////////////////////////////////////////////////////////////////
  // Action Methods
  ////////////////////////////////////////////////////////////////
  
  action void query()
  {
    tempPointQuery.copyFromStr(pointQuery.toStr(), 100);
	
	// Count total records that contains similar pointQuery
	pointQuerySize := eacio.getRecordCount(pointQuery.toStr());
	
	pointQueryStatus := (pointQuerySize == 1)?(true):(false);
	
	// Get channel number
	channel := eacio.resolveChannel(pointQuery.toStr());
	
	if (channel != 0)
	{
	  //Enabled?
	  enabled := eacio.isChannelEnabled(channel);
	  
	  // Update sedonaId and sedonaType tag
	  eacio.writeSedonaId(channel, this.id);
	  eacio.writeSedonaType(channel, this.type.kit.name, this.name);
	  
	  // Update channel name
	  // FIX: Updating Buf in native code do not update the Buf member 'size'.
	  // Below fix updates the size of Buf according to size of string.
	
	  eacio.getChannelName(channel, channelName.toStr());
	  channelName.copyFromStr(channelName.toStr());
	  changed(AnalogOutput.channelName);
	  
	  out := relinquishDefault;
	}
  }
	
  // ------Overides methods------	
  
  virtual override void start()
  {
	ticks = Sys.ticks();
  }
  
  virtual override void execute() 
  { 
    // TODO: As per SedonaDev documentation, No guarantee is made whether the 
	// string (returned by toStr()) is actually null terminated.
	
    if (!tempPointQuery.equals(pointQuery.toStr()))
	{
		tempPointQuery.copyFromStr(pointQuery.toStr(), 100);
		
		// Count total records that contains similar pointQuery
		pointQuerySize := eacio.getRecordCount(pointQuery.toStr());
		
		pointQueryStatus := (pointQuerySize == 1)?(true):(false);
		
		// Get channel number
		channel := eacio.resolveChannel(pointQuery.toStr());
		
		if (channel != 0)
		{
		  //Enabled?
		  enabled := eacio.isChannelEnabled(channel);
		  
		  // Update sedonaId and sedonaType tag
		  eacio.writeSedonaId(channel, this.id);
		  eacio.writeSedonaType(channel, this.type.kit.name, this.name);
		  
		  // Update channel name
		  // FIX: Updating Buf in native code do not update the Buf member 'size'.
		  // Below fix updates the size of Buf according to size of string.
		
		  eacio.getChannelName(channel, channelName.toStr());
		  channelName.copyFromStr(channelName.toStr());
		  changed(AnalogOutput.channelName);
		  
		  out := relinquishDefault;
		}
	}
	
		//Update curStatus every interval
	if (Sys.ticks() > (ticks + 2sec))
	{
	  // Update for next trigger
	  ticks = Sys.ticks();
	  
	  if(channel!=0)
	  {
	    // enabled slot
	    enabled := eacio.isChannelEnabled(channel);
		
	    // curStatus
	    eacio.getCurStatus(channel, curStatus.toStr());
		curStatus.copyFromStr(curStatus.toStr());
		changed(AnalogOutput.curStatus);
		
		//Level
		int level=0;
	    float inValue;
	    
	    level=eacio.getLevel(channel);
	    if(level!=0)
	    {
		  //TODO: How to determine whether value is null or 0.0,
		  // if you receive 0.0 as input?
	      inValue=eacio.getLevelValue(channel,level);
		  
		  //TODO: Below is magic number and Important. Keep insync with Haystack code
		  if(inValue<=-8888f)
		  {
		    inValue=null;
		  }
		  
		  switch(level)
		  {
		    case 1:
		      in1:=inValue;
		  	break;
		    case 2:
		      in2:=inValue;
		  	break;
		    case 3:
		      in3:=inValue;
		  	break;
		    case 4:
		      in4:=inValue;
		  	break;
		    case 5:
		      in5:=inValue;
		  	break;
		    case 6:
		      in6:=inValue;
		  	break;
		    case 7:
		      in7:=inValue;
		  	break;
		    case 8:
		      in8:=inValue;
		  	break;
		    case 9:
		      in9:=inValue;
		  	break;
		    case 10:
		      in10:=inValue;
		  	break;
		    case 11:
		      in11:=inValue;
		  	break;
		    case 12:
		      in12:=inValue;
		  	break;
		    case 13:
		      in13:=inValue;
		  	break;
		    case 14:
		      in14:=inValue;
		  	break;
		    case 15:
		      in15:=inValue;
		  	break;
		    case 16:
		      in16:=inValue;
		  	break;
		    case 17:
		      relinquishDefault:=inValue;
		  	break;
		  }
		  
		  //This is fix. When any input is set to null changed function is not called. Why?
		  //TODO: FIx this.
		  if((in1 == null) && (in2 == null) && (in3 == null) && (in4 == null) && (in5 == null) && (in6 == null) && (in7 == null) && (in8 == null) && (in9 == null) && (in10 == null) && (in11 == null) && (in12 == null) && (in13 == null) && (in14 == null) && (in15 == null) && (in16 == null))
  		  {
		    out := relinquishDefault;
		    analogOutPoint.set(channel, relinquishDefault);
		  }
	    }
	  }
	}
  }
  
  override void setToDefault(Slot slot) 
  {
	super.setToDefault(slot);
	
	if(slot.name == "in1")
	{
	  in1 := null;
	}
	else if(slot.name == "in2")
	{
	  in2 := null;
	}
	else if(slot.name == "in3")
	{
	  in3 := null;
	}
	else if(slot.name == "in4")
	{
	  in4 := null;
	}
	else if(slot.name == "in5")
	{
	  in5 := null;
	}
	else if(slot.name == "in6")
	{
	  in6 := null;
	}
	else if(slot.name == "in7")
	{
	  in7 := null;
	}
	else if(slot.name == "in8")
	{
	  in8 := null;
	}
	else if(slot.name == "in9")
	{
	  in9 := null;
	}
	else if(slot.name == "in10")
	{
	  in10 := null;
	}
	else if(slot.name == "in11")
	{
	  in11 := null;
	}
	else if(slot.name == "in12")
	{
	  in12 := null;
	}
	else if(slot.name == "in13")
	{
	  in13 := null;
	}
	else if(slot.name == "in14")
	{
	  in14 := null;
	}
	else if(slot.name == "in15")
	{
	  in15 := null;
	}
	else if(slot.name == "in16")
	{
	  in16 := null;
	}
	
	// If all 'null' set relinquishDefault
	if((in1 == null) && (in2 == null) && (in3 == null) && (in4 == null) && (in5 == null) && (in6 == null) && (in7 == null) && (in8 == null) && (in9 == null) && (in10 == null) && (in11 == null) && (in12 == null) && (in13 == null) && (in14 == null) && (in15 == null) && (in16 == null))
    {
	  out := relinquishDefault;
	  analogOutPoint.set(channel, relinquishDefault);
    }
  }

  virtual override void changed(Slot slot)
  {
	super.changed(slot);
	
	if (channel != 0)
	{
	  if(slot.name == "out")
	  {
	    analogOutPoint.set(channel, out);
	  }
	  else if((in1 != null) && (enabled==true))
	  {
	    out := in1;
	  }
	  else if((in2 != null) && (enabled==true))
	  {
	    out := in2;
	  }
	  else if((in3 != null) && (enabled==true))
	  {
	    out := in3;
	  }
	  else if((in4 != null) && (enabled==true))
	  {
	    out := in4;
	  }
	  else if((in5!= null) && (enabled==true))
	  {
	    out := in5;
	  }
	  else if((in6 != null) && (enabled==true))
	  {
	    out := in6;
	  }
	  else if((in7 != null) && (enabled==true))
	  {
	    out := in7;
	  }
	  else if((in8 != null) && (enabled==true))
	  {
	    out := in8;
	  }
	  else if((in9 != null) && (enabled==true))
	  {
	    out := in9;
	  }
	  else if((in10 != null) && (enabled==true))
	  {
	    out := in10;
	  }
	  else if((in11 != null) && (enabled==true))
	  {
	    out := in11;
	  }
	  else if((in12 != null) && (enabled==true))
	  {
	    out := in12;
	  }
	  else if((in13 != null) && (enabled==true))
	  {
	    out := in13;
	  }
	  else if((in14 != null) && (enabled==true))
	  {
	    out := in14;
	  }
	  else if((in15 != null) && (enabled==true))
	  {
	    out := in15;
	  }
	  else if((in16 != null) && (enabled==true))
	  {
	    out := in16;
	  }
	  else if (slot.name == "relinquishDefault")
	  {
	    out := relinquishDefault;
	  }
	}
  }
}
