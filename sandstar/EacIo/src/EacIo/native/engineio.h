// engineio.h

#ifndef __ENGINEIO_H__
#define __ENGINEIO_H__

// Include files

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "/home/eacio/sandstar/engine/src/engine.h"

// Definitions

#define MAX_WRITELEVELS		17
#define MAX_WRITEWHO		16

// Write level

struct _CHANNEL_WRITELEVEL
{
	int nUsed;

	double fValue;
	double fDuration;

	char sWho[MAX_WRITEWHO+1];
};

typedef struct _CHANNEL_WRITELEVEL CHANNEL_WRITELEVEL;

// Public functions

int engineio_init();
int engineio_exit();

int engineio_main();
int engineio_stop();

int engineio_enum_start(int *cenum);
int engineio_enum_next(int *cenum,ENGINE_CHANNEL *channel,ENGINE_VALUE *value);
int engineio_enum_end();

int engineio_getwritelevels(ENGINE_CHANNEL channel,CHANNEL_WRITELEVEL **levels);
int engineio_setwritelevel(ENGINE_CHANNEL channel,int nLevel,int nUsed,double fValue,double fDuration,const char *sWho,CHANNEL_WRITELEVEL **pCurrent,int *pnCurrent);

int engineio_read_channel(ENGINE_CHANNEL channel,ENGINE_VALUE *value);
int engineio_write_channel(ENGINE_CHANNEL channel,ENGINE_VALUE *value);

//int engineio_write_virtual(ENGINE_CHANNEL channel,ENGINE_VALUE *value);

// Connection to points.cpp

int engineio_signal_trigger(ENGINE_CHANNEL channel,ENGINE_VALUE *value);
int engineio_signal_writeack(ENGINE_CHANNEL channel,ENGINE_VALUE *value);

int temp_channel_add(ENGINE_CHANNEL channel);

//int engine_restart();

#endif // __ENGINEIO_H__
