/** 
 * This file contains Sedona native C binding functions.
*/

#include "sedona.h"
#include <iostream>
#include <fstream>
#include <string>

// #include <plog/Log.h>

using namespace std;

/**************************** Function Declaration ***************************/

// Sedona Interface to write integer curVal of point
void sedonaWriteCurVal(const int channel, double value);

// Sedona Iterface to read curVal of Point
int sedonaReadCurVal(const int channel, double * curVal);

// Resolve channel ID from marker list
int sedonaResolveChannel(char* markerList);

// Get record count
int sedonaGetRecordCount(char* markerList);

// Get current status of channel
int sedonaGetCurStatus(const int channel, char* status);

// Get channel name
int sedonaGetChannelName (const int channel, char* channelName);

// Write Virtual channels
//void sedonaWriteVirtualVal(const int channel, double value);

// Write sedona component id to tag
void sedonaWriteComponentId(const int channel, double value);

// Write Sedona Component Type to tag
void sedonaWriteComponentType(const int channel, char* kit, char* name);

// Get channel operational state
bool sedonaIsChannelEnabled(const int channel);

// Get bool tag value
bool sedonaGetBoolTagValue(int channel, char * tag);

// Get number tag value
float sedonaGetNumberTagValue(int channel, char * tag);

// Get string tag value
void sedonaGetStringTagValue(int channel, char * tag, char * val);

// Get Tag Type
char sedonaGetTagType(int channel, char * tag);

int sedonaGetWriteLevels(int channel);
double sedonaGetWriteLevelsValue(int channel, int level);

/********************************* Globals ***********************************/


/**************************** Function Definition ***************************/

extern "C" Cell EacIo_boolInPoint_get(SedonaVM* vm, Cell* params)
{
	Cell result;
	double curVal = 0.0;
	
	int32_t channel = params[0].ival;
	
	// Return 'false' if error
	if ((-1) == sedonaReadCurVal(channel, &curVal))
		result.ival = 0;
	
	result.ival = ((curVal == 0) ? (0) : (1));
	return result;
}

extern "C" Cell EacIo_boolOutPoint_set(SedonaVM* vm, Cell* params)
{
	int32_t channel = params[0].ival;
	int32_t channelValue = params[1].ival;
	
	Cell result;
	result.ival = 0;
   
	// select whether it is on, off or flash
	sedonaWriteCurVal(channel, (channelValue==1) ? (1) : (0));

	return result;
}

extern "C" Cell EacIo_binaryValuePoint_set(SedonaVM* vm, Cell* params)
{
	int32_t channel      = params[0].ival;
	int32_t channelValue = params[1].ival;
	
	Cell result;
	result.ival = 0;
   
	// select whether it is on, off or flash
	//sedonaWriteVirtualVal(channel, (channelValue==1) ? (1) : (0));
	sedonaWriteCurVal(channel, (channelValue==1) ? (1) : (0));
	
	return result;
}

extern "C" Cell EacIo_analogInPoint_get(SedonaVM* vm, Cell* params)
{
	Cell result;
	double curVal;
	int32_t channel = params[0].ival;
	
	sedonaReadCurVal(channel, &curVal);
	result.fval = curVal;
	//LOG_DEBUG << "channel: " << channel << " f_value: " << result.fval << " d_value: " << curVal;
	return result;
}

extern "C" Cell EacIo_analogOutPoint_set(SedonaVM* vm, Cell* params)
{
	int32_t channel      = params[0].ival;
	double  channelValue = params[1].fval;
	
	Cell result;
	result.ival = 0;
   
	sedonaWriteCurVal(channel, channelValue);

	return result;
}

extern "C" Cell EacIo_analogValuePoint_set(SedonaVM* vm, Cell* params)
{
	int32_t channel      = params[0].ival;
	double  channelValue = params[1].fval;
	
	Cell result;
	result.ival = 0;
   
	//sedonaWriteVirtualVal(channel, channelValue);
	sedonaWriteCurVal(channel, channelValue);
	
	return result;
}

extern "C" Cell EacIo_triacPoint_set(SedonaVM* vm, Cell* params)
{
	int32_t channel =(params[0].ival);
	int32_t cmd     = params[1].ival;
	
	Cell result;
	result.ival = 0;
   
	// select whether it is on, off or flash
	sedonaWriteCurVal(channel, (cmd==1) ? (1) : (0));

	return result;
}

extern "C" Cell EacIo_eacio_resolveChannel(SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	char* markerList = static_cast <char *> (params[0].aval);
	
	result.ival = sedonaResolveChannel(markerList);
	//LOG_DEBUG << result.ival;
	
	return result;
}

extern "C" Cell EacIo_eacio_getRecordCount(SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	char* markerList = static_cast <char *> (params[0].aval);
	
	result.ival = sedonaGetRecordCount(markerList);
	//LOG_DEBUG << result.ival;
	
	return result;
}

extern "C" Cell EacIo_eacio_getCurStatus(SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	int32_t channel   = params[0].ival;
	char * curStatus = static_cast <char *> (params[1].aval);
	
	result.ival = sedonaGetCurStatus(channel, curStatus);
	//LOG_DEBUG << curStatus;
	
	return result;
}

extern "C" Cell EacIo_eacio_getChannelName (SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	int32_t channel    = params[0].ival;
	char * channelName = static_cast <char *> (params[1].aval);
	
	
	result.ival = sedonaGetChannelName(channel, channelName);
	//LOG_DEBUG << channelName;
	
	return result;
}

extern "C" Cell EacIo_eacio_writeSedonaId (SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	int channel = params[0].ival;
	int id      = params[1].ival;
	
	sedonaWriteComponentId(channel, id);
	
	return result;
}

extern "C" Cell EacIo_eacio_writeSedonaType (SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	int channel = params[0].ival;
	char* kit   = static_cast <char *> (params[1].aval);
	char* name  = static_cast <char *> (params[2].aval);
	
	sedonaWriteComponentType(channel, kit, name);
	
	return result;
}

extern "C" Cell EacIo_eacio_isChannelEnabled(SedonaVM* vm, Cell* params)
{
	Cell result;
	result.ival = 0;
	
	int32_t channel    = params[0].ival;
	
	result.ival = sedonaIsChannelEnabled(channel);
	//LOG_DEBUG << channel << " " << result.ival;
	
	return result;
}

extern "C" Cell EacIo_eacio_getBoolTagValue(SedonaVM* vm, Cell* params)
{
	Cell result;
	
	int channel = params[0].ival;
	char * tag = static_cast <char *> (params[1].aval);
	
	//LOG_DEBUG << "channel: " << channel;
	//LOG_DEBUG << "tag: " << tag;
	result.ival = sedonaGetBoolTagValue(channel, tag);
	
	return result;
}

extern "C" Cell EacIo_eacio_getNumberTagValue(SedonaVM* vm, Cell* params)
{
	Cell result;
	
	int channel = params[0].ival;
	char * tag = static_cast <char *> (params[1].aval);
	
	result.fval = sedonaGetNumberTagValue(channel, tag);
	//LOG_DEBUG << "channel: " << channel << " f_value: " << result.fval << " tag: " << tag;
	return result;
}

extern "C" Cell EacIo_eacio_getStringTagValue(SedonaVM* vm, Cell* params)
{
	Cell result;
	
	int channel = params[0].ival;
	char * tag = static_cast <char *> (params[1].aval);
	char * val = static_cast <char *> (params[2].aval);
	
	sedonaGetStringTagValue(channel, tag, val);
	//LOG_DEBUG << "channel: " << channel << " tag: " << tag << " val: " << val;
	return result;
}

extern "C" Cell EacIo_eacio_getTagType(SedonaVM* vm, Cell* params)
{
	Cell result;
	
	int channel = params[0].ival;
	char * tag = static_cast <char *> (params[1].aval);
	
	result.ival = sedonaGetTagType(channel, tag);
	
	//LOG_DEBUG << "channel: " << channel << " f_value: " << result.fval << " tag: " << tag;
	return result;
}

extern "C" Cell EacIo_eacio_getLevel(SedonaVM* vm, Cell* params)
{
	Cell result;
	
	int channel = params[0].ival;
	result.ival = sedonaGetWriteLevels(channel);
	// LOG_DEBUG << "channel: " << channel << " level: " << result.ival;
	
	return result;
}

extern "C" Cell EacIo_eacio_getLevelValue(SedonaVM* vm, Cell* params)
{
	Cell result;
	
	int channel = params[0].ival;
	int level = params[1].ival;
	
	result.fval = sedonaGetWriteLevelsValue(channel, level);
	// LOG_DEBUG << "channel: " << channel << " f_value: " << result.fval << " level: " << level;
	
	return result;
}
//static native bool getStatus(int channel, AnalogInputStatusData data);
//static native bool setScaleLow(int channel, float scaleLow);
//static native bool setScaleHigh(int channel, float scaleHigh);
//static native bool setLowCutOff(int channel, float lowCutOff);
//static native bool setOffset(int channel, float offset);
//static native bool setDigitalOnLevel(int channel, float level);
//static native bool setDigitalOffLevel(int channel, float level);
//static native bool setInputType(int channel, int type);
//static native bool setTempTable(int channel, int table);
//static native bool setDecimalPoint(int channel, int point);
//static native bool setCutOffEnable(int channel, bool bEnable);
//static native bool setLinearization(int channel, bool bSquareroot);
//static native bool resetMaximumValue(int channel);
//static native bool resetMinimumValue(int channel);
//static native int getAlarm(int channel);
//static native bool setAlarmHighEnable(int channel, bool bEnable);
//static native bool setAlarmLowEnable(int channel, bool bEnable);
//static native bool setAlarmHighLimit(int channel, float value);
//static native bool setAlarmLowLimit(int channel, float value);
//static native bool setAlarmDeadband(int channel, float value);
//static native bool setAlarmDelayTime(int channel, int time);
//static native bool setAlarmResetType(int channel, bool bAuto);
//static native bool resetAlarm(int channel);
