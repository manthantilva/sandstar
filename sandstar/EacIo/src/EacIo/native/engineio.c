// engineio.c

// Include files

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>

#include "engineio.h"

// Definitions

#define MAX_CHANNELS	      32
//#define MAX_VIRTUAL_CHANNELS  10

#define CLIENT_KEY		0x45414331

// Channel item

struct _CHANNEL_ITEM
{
	int nUsed;
	int nDirty;
	int nLevel;

	ENGINE_CHANNEL channel;
	ENGINE_VALUE value;

	CHANNEL_WRITELEVEL *levels;
};

typedef struct _CHANNEL_ITEM CHANNEL_ITEM;

// Channel struct

struct _CHANNEL
{
	int nItems;
	int nCount;

	CHANNEL_ITEM *items;
};

typedef struct _CHANNEL CHANNEL;

// Local functions

static int engineio_error(char *sFormat,...);

static int engineio_lock();
static int engineio_unlock();

// Channel functions

static int channel_init(CHANNEL *channels,int nItems);
static int channel_exit(CHANNEL *channels);

static int channel_add(CHANNEL *channels,ENGINE_CHANNEL channel);

static int channel_change(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value);
static int channel_writeack(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value);

static int channel_find(CHANNEL *channels,ENGINE_CHANNEL channel);
static int channel_alloc(CHANNEL *channels);

static int channel_levels(CHANNEL_ITEM *item);

// Value functions

static int value_status(ENGINE_VALUE *value,ENGINE_STATUS status);

// Message functions

static int send_message(int hQueue,int nMessage,ENGINE_CHANNEL channel,ENGINE_VALUE *value);

// vm linkage

// int willTerminate_c();

// Local data

static CHANNEL g_channels;

static int g_hEngine=-1;
static int g_hQueue=-1;

static pthread_mutex_t g_lock;

static int g_intEngineRunning = 1;
// Public functions

// engineio_init
// Call to initialize engine
int engineio_pre_init()
{
	int temp = msgget(ENGINE_MESSAGE_KEY,0666);
	if(temp > 0)
	{
		if(msgctl(temp,IPC_RMID,0)<0)
		{
			printf("failed to destroy Engine Queue");
			// return -1;
		}

	// engineio_error("engine not started");
		// return -1;
    }
	temp = msgget(CLIENT_KEY,0666);
	if(temp > 0)
	{
		if(msgctl(temp,IPC_RMID,0)<0)
		{
			printf("failed to destroy Client Queue");
			// return -1;
		}
    }

}
	
int engineio_init()
{
	// init channels
	channel_init(&g_channels,MAX_CHANNELS);

	// engine queue found?
	g_hEngine=msgget(ENGINE_MESSAGE_KEY,0666);

	if(g_hEngine<0)
	{
		engineio_error("engine not started");
		return -1;
    }

	// create queue?
	g_hQueue=msgget(CLIENT_KEY,IPC_CREAT|0666);

	if(g_hQueue<0)
	{
		engineio_error("failed to create message queue");
		return -1;
    }

	// ask for notifications
	send_message(g_hEngine,ENGINE_MESSAGE_NOTIFY,0,NULL);

	// create a mutex for it
	if(pthread_mutex_init(&g_lock,NULL)<0)
	{
		engineio_error("failed to create mutex");
		return -1;
	}
	g_intEngineRunning = 1;
	return 0;
}

// engineio_exit
// Call to uninitialize engine

int engineio_exit()
{
	// destroy mutex
	pthread_mutex_destroy(&g_lock);

	// release channels
	channel_exit(&g_channels);

	// stop notifications
	if(g_hEngine>=0){
		// printf("Sending ENGINE_MESSAGE_UNNOTIFY msg to engine\n");
		send_message(g_hEngine,ENGINE_MESSAGE_UNNOTIFY,0,NULL);
		// printf("Sent ENGINE_MESSAGE_UNNOTIFY msg to engine\n");
	}

	// kill queue
	if(g_hQueue>=0 && msgctl(g_hQueue,IPC_RMID,0)<0)
	{
		engineio_error("failed to destroy message queue");
		return -1;
	}

	// remove queues
	g_hEngine=-1;
	g_hQueue=-1;
	g_intEngineRunning = 0;

	return 0;
}

// engineio_main
// Called by engine thread

int engineio_main()
{
	// queue exists?
	if(g_hQueue<0) return -1;

	// read messages
	int stop=0;
	g_intEngineRunning = 1;

	
	
	
	
	while(stop==0 && g_intEngineRunning)
	{
		ENGINE_MESSAGE msg;
		
		int err=msgrcv(g_hQueue,&msg,ENGINE_MESSAGE_SIZE,0,0);
		//int err=msgrcv(g_hQueue,&msg,ENGINE_MESSAGE_SIZE,0,IPC_NOWAIT);
		
		if(err>=0)
		{
			// lock down
			engineio_lock();

			// what message?
			switch(msg.nMessage)
			{
				case ENGINE_MESSAGE_STOP:
					// stop message
					// printf("Stop in app\n");
					stop=1;
					break;

				case ENGINE_MESSAGE_CHANGE:
					// change message
					channel_change(&g_channels,msg.channel,&msg.value);
					break;

				case ENGINE_MESSAGE_WRITEACK:
					// write ack message
					channel_writeack(&g_channels,msg.channel,&msg.value);
					break;

				default:
					// unknown message
					engineio_error("received unknown message (0x%-2.2lX)",msg.nMessage);
					break;
			}

			// release lock
			engineio_unlock();
		}
	}
	engineio_exit();
	return 0;
}

// engineio_stop
// Call to send stop message to us

int engineio_stop()
{
	// queue exists?
	// printf("engineio_stop in app\n");
	if(g_hQueue<0) return -1;
	// printf("engineio_stop in app\n");
	
	g_intEngineRunning = 0;

	// send us a stop mesage
	return send_message(g_hQueue,ENGINE_MESSAGE_STOP,0,NULL);
}

// engineio_enum_start
// Call to enumerate dirty channels

int engineio_enum_start(int *cenum)
{
	// lock down
	engineio_lock();

	// find first?
	int n;

	for(n=0;n<g_channels.nItems;n++)
	{
		// used and dirty?
		if(g_channels.items[n].nUsed!=0 &&
			g_channels.items[n].nDirty!=0)
		{
			*cenum=n;
			return 0;
		}
	}

	// none found
	*cenum=-1;

	return -1;
}

// engineio_enum_next
// Call to enumerate next dirty channel

int engineio_enum_next(int *cenum,ENGINE_CHANNEL *channel,ENGINE_VALUE *value)
{
	// end of list?
	int n=*cenum;

	if(n>=0)
	{
		// pass back current
		CHANNEL_ITEM *item=&g_channels.items[n];

		*channel=item->channel;
		*value=item->value;

		item->nDirty=0;

		// find next?
		while(++n<g_channels.nItems)
		{
			// used and dirty?
			if(g_channels.items[n].nUsed!=0 &&
				g_channels.items[n].nDirty!=0)
			{
				*cenum=n;
				return 0;
			}
		}

		// none found
		*cenum=-1;

		return 0;
	}
	return -1;
}

// engineio_enum_end
// Call to end channel enumeration

int engineio_enum_end()
{
	// release lock
	engineio_unlock();
}

// engineio_getwritelevels
// Call to get write level info for channel

int engineio_getwritelevels(ENGINE_CHANNEL channel,CHANNEL_WRITELEVEL **levels)
{
	// find channel
	int n=channel_find(&g_channels,channel);

	if(n>=0)
	{
		CHANNEL_ITEM *item=&g_channels.items[n];

		if(channel_levels(item)>=0)
		{
			// get levels
			*levels=item->levels;
			return 0;
		}
	}
	return -1;
}

// engineio_setwritelevel
// Call to set write level for channel

int engineio_setwritelevel(ENGINE_CHANNEL channel,int nLevel,int nUsed,double fValue,double fDuration,const char *sWho,CHANNEL_WRITELEVEL **pCurrent,int *pnCurrent)
{
	// find channel
	int n=channel_find(&g_channels,channel);

	if(n>=0)
	{
		CHANNEL_ITEM *item=&g_channels.items[n];

		if(channel_levels(item)>=0)
		{
			// set level info
			nLevel--;
			CHANNEL_WRITELEVEL *level=&item->levels[nLevel];

			level->nUsed=nUsed;

			level->fValue=fValue;
			level->fDuration=fDuration;

			if(sWho!=NULL) strncpy(level->sWho,sWho,MAX_WRITEWHO);

			// get current level
			*pCurrent=NULL;
			*pnCurrent=MAX_WRITELEVELS;

			if(nUsed==1)
			{
				// new level overrides current?
				if(nLevel<=item->nLevel)
				{
					*pCurrent=level;
					*pnCurrent=nLevel+1;

					item->nLevel=nLevel;
				}
			}
			else
			{
				// find next highest level
				int n=item->nLevel+1;

				while(n<MAX_WRITELEVELS)
				{
					if(item->levels[n].nUsed!=0)
					{
						*pCurrent=&item->levels[n];
						*pnCurrent=n+1;

						item->nLevel=n;
						break;
					}
					n++;
				}
			}
			return 0;
		}
	}
	return -1;
}

// engineio_read_channel
// Call to read channel value

int engineio_read_channel(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// lock down
	engineio_lock();

	int err=-1;

	// queues exist?
	if(g_hEngine>=0 && g_hQueue>=0)
	{
		// channel found?
		int n=channel_find(&g_channels,channel);

		if(n>=0)
		{
			// get current value
			*value=g_channels.items[n].value;
			err=0;
		}
	}

	// read failed?
	if(err<0) value_status(value,ENGINE_STATUS_FAULT);

	// release lock
	engineio_unlock();

	return err;
}

// engineio_write_channel
// Call to write channel value

int engineio_write_channel(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// lock down
//	engineio_lock();

	int err=-1;

	// queues exist?
	if(g_hEngine>=0 && g_hQueue>=0)
	{
		// send write message
		err=send_message(g_hEngine,ENGINE_MESSAGE_WRITE,channel,value);
	}

	// write failed?
	if(err<0) value_status(value,ENGINE_STATUS_FAULT);

	// release lock
//	engineio_unlock();

	return -1;
}

// engineio_write_virtual
// Call to write channel value

//int engineio_write_virtual(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
//{
//	// lock down
////	engineio_lock();
//
//	int err=-1;
//
//	// queues exist?
//	if(g_hEngine>=0 && g_hQueue>=0)
//	{
//		// send write message
//		err=send_message(g_hEngine,ENGINE_MESSAGE_WRITE_VIRTUAL,channel,value);
//	}
//
//	// write failed?
//	if(err<0) value_status(value,ENGINE_STATUS_FAULT);
//
//	// release lock
////	engineio_unlock();
//
//	return -1;
//}

// Local functions

// engineio_error
// Call to display engine error

static int engineio_error(char *sFormat,...)
{
    va_list args;

	// output error
	printf("-- ERROR [sys::Engine] ");

	va_start(args,sFormat);
	vprintf(sFormat,args);
	va_end(args);

	printf("\n");

	return -1;
}

// engineio_lock
// Call to set channel lock

static int engineio_lock()
{
	return pthread_mutex_lock(&g_lock);
}

// engineio_unlock
// Call to release channel lock

static int engineio_unlock()
{
	return pthread_mutex_unlock(&g_lock);
}

// Channel functions

// channel_init
// Call to initialize channel controller

static int channel_init(CHANNEL *channels,int nItems)
{
	int nSize=nItems*sizeof(CHANNEL_ITEM);

	channels->nItems=nItems;
	channels->nCount=0;

	channels->items=(CHANNEL_ITEM *) malloc(nSize);
	memset(channels->items,0,nSize);

	return 0;
}

// channel_exit
// Call to release channel controller

static int channel_exit(CHANNEL *channels)
{
	// release levels
	int n;

	for(n=0;n<channels->nItems;n++)
	{
		CHANNEL_ITEM *item=&channels->items[n];

		if(item->nUsed!=0 && item->levels!=NULL)
		{
			free(item->levels);
			item->levels=NULL;
		}
	}

	// free items?
	if(channels->items!=NULL)
	{
		free(channels->items);
		channels->items=NULL;
	}

	channels->nItems=0;
	channels->nCount=0;

	return 0;
}

// channel_add
// Call to add item to channel list

static int channel_add(CHANNEL *channels,ENGINE_CHANNEL channel)
{
	// already added?
	int n=channel_find(channels,channel);
	if(n>=0) return n;

	// get next free
	n=channel_alloc(channels);

	if(n<0)
	{
		engineio_error("out of channel space");
		return -1;
	}

	// enable channel item
	CHANNEL_ITEM *item=&channels->items[n];

	item->nUsed=1;
	item->nDirty=1;
	item->nLevel=MAX_WRITELEVELS-1;

	item->channel=channel;

	// initial value
	value_status(&item->value,ENGINE_STATUS_UNKNOWN);

	// add to count
	channels->nCount++;

	return n;
}

int temp_channel_add(ENGINE_CHANNEL channel)
{
	CHANNEL *channels = &g_channels;
	
	// already added?
	int n=channel_find(channels,channel);
	if(n>=0) return n;

	// get next free
	n=channel_alloc(channels);

	if(n<0)
	{
		engineio_error("out of channel space");
		return -1;
	}

	// enable channel item
	CHANNEL_ITEM *item=&channels->items[n];

	item->nUsed=1;
	item->nDirty=1;
	item->nLevel=MAX_WRITELEVELS-1;

	item->channel=channel;

	// add to count
	channels->nCount++;

	return n;
}

// channel_change
// Call to update current values

static int channel_change(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// add the channel?
	int n=channel_add(channels,channel);

	if(n>=0)
	{
		// update its value
		CHANNEL_ITEM *item=&channels->items[n];

		item->value=*value;
		item->nDirty=1;

		// has a trigger?
		if(value->trigger>0)
			engineio_signal_trigger(channel,value);

		return 0;
	}
	return -1;
}

// channel_writeack
// Call to process write acks

static int channel_writeack(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// add the channel?
	int n=channel_add(channels,channel);

	if(n>=0)
	{
		engineio_signal_writeack(channel,value);
		return 0;
	}
	return -1;
}

// channel_find
// Call to find existing channel item

static int channel_find(CHANNEL *channels,ENGINE_CHANNEL channel)
{
	int n;

	for(n=0;n<channels->nItems;n++)
	{
		// item found?
		CHANNEL_ITEM *item=&channels->items[n];

		if(item->nUsed!=0 && item->channel==channel)
			return n;
	}
	return -1;
}

// channel_alloc
// Call to find first free channel item

static int channel_alloc(CHANNEL *channels)
{
	int n;

	for(n=0;n<channels->nItems;n++)
	{
		// item not in use?
		if(channels->items[n].nUsed==0)
			return n;
	}
	return -1;
}

// channel_levels
// Call to initialize write levels

static int channel_levels(CHANNEL_ITEM *item)
{
	// lazy init?
	if(item->levels==NULL)
	{
		// allocate levels for channel
		int nSize=sizeof(CHANNEL_WRITELEVEL)*MAX_WRITELEVELS;
		item->levels=(CHANNEL_WRITELEVEL *) malloc(nSize);

		if(item->levels==NULL)
		{
			engineio_error("out of memory creating channel write levels");
			return -1;
		}

		// initialize levels
		memset(item->levels,0,nSize);
	}
	return 0;
}

// Value functions

// value_status
// Call to set value to status

int value_status(ENGINE_VALUE *value,ENGINE_STATUS status)
{
	// set unknown status
	value->status=status;

	value->raw=(ENGINE_DATA) -1;
	value->cur=(ENGINE_DATA) -1;

	value->flags=0;
	value->trigger=0;

	return 0;
}

// Message functions

// send_message
// Call to send engine message

static int send_message(int hQueue,int nMessage,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// prep message
	ENGINE_MESSAGE msg;
	memset(&msg,0,sizeof(ENGINE_MESSAGE));

	msg.nMessage=nMessage;
	msg.channel=channel;
	msg.sender=CLIENT_KEY;

	if(value!=NULL) msg.value=*value;

	// put message on queue?
	int err=msgsnd(hQueue,&msg,ENGINE_MESSAGE_SIZE,0);
	if(err<0) engineio_error("failed to send message");
	return err;
}

// engine_restart
// Call to reboot engine

/*
int engine_restart()
{
	int err=-1;

	// queues exist?
	if(g_hEngine>=0 && g_hQueue>=0)
	{
		// send write message
		err=send_message(g_hEngine,ENGINE_MESSAGE_RESTART,0,NULL);
	}

	return err;
}
*/