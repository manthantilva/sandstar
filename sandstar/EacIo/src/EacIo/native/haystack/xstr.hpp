#pragma once

/**
 * XStr is an extended string which is a type name and value
 * encoded as a string. It is used as a generic value when an
 * XStr is decoded without any predefined type.
 */

#include "val.hpp"
#include "bin.hpp"

namespace haystack {
    /**
     XStr models a Extended String.

     @see <a href='http://project-haystack.org/doc/TagModel#tagKinds'>Project Haystack</a>

     */
    class XStr : public Val
    {
        XStr();
        // disable assignment
        XStr& operator = (const XStr &other);
        XStr(const XStr& other) : xstr_type(other.xstr_type), value(other.value) {};
    public:
        const Type type() const { return XSTR_TYPE; }

		bool isValidType(std::string t);
		
        /**
        This string type
        */
        std::string xstr_type;
        /**
        This string value
        */
        std::string value;
        /**
        Construct from std::string
        */
        XStr(const std::string type, const std::string val);

        /**
        MIME type
        */
        const std::string to_string() const;

        /**
        Encode as Type("Value")
        */
        const std::string to_zinc() const;

        /**
        Equality is value based
        */
        bool operator == (const XStr &b) const;
		bool operator == (const Val &other) const;
		bool operator == (const std::string &other) const;
        bool operator > (const Val &other) const;
        bool operator < (const Val &other) const;

        auto_ptr_t clone() const;

        //DENKO
	void *decode(std::string type, const std::string &val);
	//Xstr XStr::encode(Object val);

    };
};
