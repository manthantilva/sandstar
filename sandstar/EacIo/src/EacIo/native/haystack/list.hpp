#pragma once

#include "val.hpp"

namespace haystack {
	/**
	 * List is an immutable list of Val items.
	 */
    class List : public Val
    {
        List();

        // disable assignment
        List& operator = (const List &other);
        List(const List& other) : items(other.items) {};
		
    public:
	
		typedef boost::ptr_vector<Val> item_t;
		typedef item_t::const_iterator const_iterator;
        
		const Type type() const { return LIST_TYPE; }
		
		item_t items;
		
        /**
        Construct from std::string
        */
		List(item_t);

        /**
        MIME type
        */
        const std::string to_string() const;

        /**
        Encode as Type("Value")
        */
        const std::string to_zinc() const;

        /**
        Equality is value based
        */
        bool operator == (const List &b) const;
	bool operator == (const Val &other) const;
	bool operator == (const std::string &other) const;

		/*DENKO*/
	bool operator > (const Val &other) const;
        bool operator < (const Val &other) const;
        

        auto_ptr_t clone() const;
    };
};
