// haystack.cpp
// 
// Copyright (c) 2015, J2 Innovations
// Author: Radu Racariu <radur@j2inn.com>
// 
// Based on (original notes):
// $Id: //poco/1.4/Net/samples/HTTPTimeServer/src/HTTPTimeServer.cpp#1 $
//
// This sample demonstrates the HTTPTimeServer and related classes.
//
// Copyright (c) 2005-2006, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

// Includes

#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/AbstractHTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/NumberFormatter.h"
#include "Poco/Exception.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/URI.h"

#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>
//#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "points.hpp"
#include "op.hpp"
#include "logger.h"

//#include "Poco/Thread.h"

// Namespaces

using Poco::Timestamp;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::ThreadPool;
using Poco::Net::StreamSocket;
using Poco::Net::ServerSocket;
using Poco::Net::SocketAddress;
using Poco::Net::TCPServer;
using Poco::Net::TCPServerConnectionFilter;
using Poco::Net::TCPServerConnection;
using Poco::Net::TCPServerConnectionFactory;
using Poco::Net::TCPServerConnectionFactoryImpl;
using Poco::Net::TCPServerParams;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;

// Sedona linkage

extern int willTerminate();

// Config Macros
#define LOCALHOST_ENABLE (0)   // 1 -> Enable  0 -> Disable

// Class definitions

// HaystackRequestHandler
//
// Created by request factory to handle all unauthenticated
// http requests made to haystack server

class HaystackRequestHandler:public HTTPRequestHandler
{
private:
	// Local data
	haystack::PointServer &m_server;

public:
	// Contruction

	HaystackRequestHandler(haystack::PointServer &server):m_server(server) {}

	// Overrides

	// handleRequest
	// Called to handle incomming request

	void handleRequest(HTTPServerRequest &request,HTTPServerResponse &response)
	{
		// get url path
		const std::string path=Poco::URI(request.getURI()).getPath();

		// output message
		// std::cout << "-- MESSAGE [sys::Haystack] " << request.getMethod() << " " << path << " from " << request.clientAddress().toString() << std::endl;
		haystack::Logger::getInstance().debug("-- MESSAGE [sys::Haystack] %s %s from %s",request.getMethod().c_str(),path.c_str(),request.clientAddress().toString().c_str());

		// no path?
		if(path=="" || path=="/")
		{
			// redirect to about
			response.redirect("/about");
			return;
		}

		// set response encoding
		response.setChunkedTransferEncoding(true);

		// get slash pos
		size_t slash=path.find('/',1);
		if(slash==path.npos) slash=path.size();

		// get op name
		std::string name=path.substr(1,slash);
		haystack::Op *op=(haystack::Op *) m_server.op(name,false);

		if(op==NULL)
		{
			// op not found
			// std::cout << "-- ERROR [sys::Haystack] op '" << name  << "' not valid" << std::endl;
			haystack::Logger::getInstance().err("-- ERROR [sys::Haystack] op '%s' not valid",name.c_str());

			response.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_NOT_FOUND);
			response.send();

			return;
		}

		// process op
		op->on_service(m_server,request,response);
	}
};

// AuthRequestHandler
//
// Created by request factory to handle all authenticated
// http requests made to haystack server through /auth

class AuthRequestHandler:public Poco::Net::AbstractHTTPRequestHandler
{
private:
	// Local data
	haystack::PointServer &m_server;

public:
	// Construction

	AuthRequestHandler(haystack::PointServer &server):m_server(server) {}

	//
	//

	void handleRequest(HTTPServerRequest &request,HTTPServerResponse &response)
	{
		AbstractHTTPRequestHandler::handleRequest(request,response);
	}

	//
	//

	void run()
	{
		// get url path
		std::string path=Poco::URI(request().getURI()).getPath();

		// output message
		// std::cout << "-- MESSAGE [sys::Haystack] auth " << request().getMethod() << " " << path << " from " << request().clientAddress().toString() << std::endl;
		haystack::Logger::getInstance().debug("-- MESSAGE [sys::Haystack] auth %s %s from %s",request().getMethod().c_str(),path.c_str(),request().clientAddress().toString().c_str());

		// no path?
		if(path=="/auth" || path=="/auth/")
		{
			// redirect to about
			response().redirect("/auth/about");
			return;
		}

		// set response encoding
        response().setChunkedTransferEncoding(true);

		// remove /auth from path
		path=path.substr(5);//path.find("/auth/")+6);

		// get slash pos
		size_t slash=path.find('/',1);
		if(slash==path.npos) slash=path.size();

		// get op name
		std::string name=path.substr(1,slash);
		haystack::Op *op=(haystack::Op *) m_server.op(name,false);

		if(op==NULL)
		{
			// op not found
			// std::cout << "-- ERROR [sys::Haystack] auth op '" << name  << "' not valid" << std::endl;
			haystack::Logger::getInstance().debug("-- ERROR [sys::Haystack] auth op '%s' not valid",name.c_str());

			response().setStatusAndReason(Poco::Net::HTTPResponse::HTTP_NOT_FOUND);
			response().send();

			return;
		}

		// process op
		op->on_service(m_server,request(),response());
	}

	//
	//

	bool authenticate()
	{
		// basic example
		if(request().hasCredentials())
		{
			//
			std::string scheme;
			std::string authinfo;

			request().getCredentials(scheme,authinfo);

// TODO: implement this

			// u: demo
			// p: demo

			if(scheme=="Basic" && authinfo=="ZGVtbzpkZW1v")
                return true;
        }
  
		//
		response().requireAuthentication("/auth");

		return false;
	}
};

// HaystackRequestHandlerFactory
//
//

class HaystackRequestHandlerFactory:public HTTPRequestHandlerFactory
{
private:
	// Local data
    haystack::PointServer &m_server;

public:
	// Construction

	HaystackRequestHandlerFactory(haystack::PointServer &server):m_server(server) {}

	// Overrides

	// createRequestHandler
	// Called to create handler for http request

	HTTPRequestHandler *createRequestHandler(const HTTPServerRequest &request)
	{
		const std::string path=Poco::URI(request.getURI()).getPath();

		// authenticated request?
		if(boost::starts_with(path,"/auth"))
			return new AuthRequestHandler(m_server);

		return new HaystackRequestHandler(m_server);
    }
};

class RejectFilter: public TCPServerConnectionFilter
{
private:
	std::vector<std::string> m_allowIPList;
public:
	
    RejectFilter()
	{
		struct stat buffer;
		
		if(stat(EACIO_FILE_ALLOWED_IP, &buffer)==0)
		{
			std::ifstream allowedIpFile(EACIO_FILE_ALLOWED_IP);
			std::copy(std::istream_iterator<std::string>(allowedIpFile), 
						std::istream_iterator<std::string>(), 
						std::back_inserter(m_allowIPList));
		}
		else
		{
			m_allowIPList.push_back("*");
			// std::cout << "-- MESSAGE [sys::Haystack] " << EACIO_FILE_ALLOWED_IP << " file not found. Adding '*' for Access-Control-Allow-Origin" << std::endl;
			haystack::Logger::getInstance().debug("-- MESSAGE [sys::Haystack] %s file not found. Adding '*' for Access-Control-Allow-Origin",EACIO_FILE_ALLOWED_IP);
		}
	}
	
	bool accept(const StreamSocket& ss)
	{
		bool result = false;
		
		//std::cout << "-- MESSAGE [sys::Haystack] " << ss.peerAddress().host().toString() << std::endl;
		
		if(std::find(m_allowIPList.begin(), m_allowIPList.end(), "*") != (m_allowIPList.end()))
		{
			result = true;
		}
		else
		{
			try
			{
				std::string origin = ss.peerAddress().host().toString();
				
				if (!origin.empty())
				{
					//std::cout << "ORIGIN: " << origin;
					for (std::vector<std::string>::iterator it = m_allowIPList.begin(), end = m_allowIPList.end();
							it!=end;
							it++)
					{
						if(it->find(origin) != std::string::npos)
						{
							result = true;
							break;
						}
					}
				}
			}
			catch (const std::exception& e)
			{
				// std::cout << "-- ERROR [sys::Haystack] " << e.what();
				haystack::Logger::getInstance().debug("-- ERROR [sys::Haystack] %s",e.what());
			}	
		}
		
		return result;
	}
};

// HTTPHaystackServer
//
// Main application object for haystack server. Starts server on specified
// port and listens for incomming requests.
//
// The configuration file must be called 'haystack.properties' and
// must reside in the svm executable directory or in one of its parents.

class HTTPHaystackServer:public Poco::Util::ServerApplication
{
public:
	// Construction
	HTTPHaystackServer() {}
	~HTTPHaystackServer() {}

protected:
	// Overrides

	//
	//

	void initialize(Application &self)
	{
		// load default configuration file
		loadConfiguration();
		ServerApplication::initialize(self);
	}

	//
	//

    void uninitialize()
    {
        ServerApplication::uninitialize();
    }

	//
	//

	int main(const std::vector<std::string> &args)
	{
		// get config parameters
#if LOCALHOST_ENABLE
		std::string listen=config().getString("haystack.listen","127.0.0.1");
#endif //LOCALHOST_ENABLE
		unsigned short port=(unsigned short)config().getInt("haystack.port",8085);

		int maxQueued=config().getInt("haystack.maxQueued",100);
		int maxThreads=config().getInt("haystack.maxThreads",16);

		//
		ThreadPool::defaultPool().addCapacity(maxThreads);

		//
		HTTPServerParams *params=new HTTPServerParams;

		params->setMaxQueued(maxQueued);
		params->setMaxThreads(maxThreads);
		params->setKeepAlive(true);

		// set-up a server socket
#if LOCALHOST_ENABLE
		ServerSocket svs(SocketAddress(listen,port));
#else //LOCALHOST_ENABLE
		ServerSocket svs(port);
#endif //LOCALHOST_ENABLE

		// set-up a HTTPServer instance
		haystack::PointServer server;
		HTTPServer srv(new HaystackRequestHandlerFactory(server),svs,params);
		srv.setConnectionFilter(new RejectFilter);

		// start server
		srv.start();
#if LOCALHOST_ENABLE
		// std::cout << "-- MESSAGE [sys::Haystack] server started on " << listen << " port=" << port << std::endl;
		haystack::Logger::getInstance().debug("-- MESSAGE [sys::Haystack] server started on %s port=%d",listen.c_str(),port);
#else //LOCALHOST_ENABLE
		// std::cout << "-- MESSAGE [sys::Haystack] server started on " << " port=" << port << std::endl;
		haystack::Logger::getInstance().debug("-- MESSAGE [sys::Haystack] server started port=%d",port);
#endif //LOCALHOST_ENABLE

		while(willTerminate()==0) sleep(1);	// TODO: fixme

        // wait for CTRL-C or kill
//        waitForTerminationRequest();

		// stop server
		srv.stop();

		return Application::EXIT_OK;
	}
};

// Sedona linkage

// haystack_main
// Called by sedona in its own thread

extern "C" int haystack_main()
{
	// needed for config file
	int argc=1;
	char *argv[1];

	argv[0]=(char *) "haystack";

	// start server app
	HTTPHaystackServer app;
	return app.run(argc,argv);
}
