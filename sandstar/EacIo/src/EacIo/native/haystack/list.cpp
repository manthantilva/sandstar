#include <stdio.h>
#include <ctype.h>
#include <sstream>
#include <stdexcept>

//#include "headers.hpp"

//#include <boost/scoped_ptr.hpp>
//#include <boost/ptr_container/ptr_map.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
//#include <boost/iterator/iterator_facade.hpp>

#include "list.hpp"

// Log
// #include <plog/Log.h>

////////////////////////////////////////////////
// List
////////////////////////////////////////////////
using namespace haystack;

List::List(item_t item)
{
	items = item;
}

const std::string List::to_string() const
{
	return to_zinc();
}

////////////////////////////////////////////////
// to zinc
////////////////////////////////////////////////

const std::string List::to_zinc() const
{
	std::stringstream os;
	bool first = true;

	//LOG_DEBUG << "List size:" << items.size();
	
	os << "[";

	for (item_t::const_iterator it = items.begin(), e = items.end(); it != e; ++it)
	{
		Val* val = { new_clone(*it) };

		if (first)
		{
			first = false;
		}
		else
		{
			os << ',';
		}			

		os << val->to_zinc();
		//LOG_DEBUG << "List loop:" << val->to_string();
	}

	os << ']';

	//LOG_DEBUG << "List:" << os.str();
	return os.str();
}

////////////////////////////////////////////////
// Equal DENKO
////////////////////////////////////////////////
bool List::operator ==(const List &other) const
{

	for (item_t::const_iterator t_it = items.begin(), t_e = items.end(), o_it = other.items.begin(), o_e = other.items.end(); t_it != t_e && o_it != o_e; ++t_it, o_it++)
	{
		Val* t_val = { new_clone(*t_it) };			
		Val* o_val = { new_clone(*o_it) };			

		if(!(t_val == o_val))
		{
			return false;
		}

	}

    return true;
}

bool List::operator==(const Val &other) const
{
    // TODO: Not yet implemented
    return false;
}

bool List::operator ==(const std::string &other) const
{
    // TODO: Not yet implemented
    return false;
}


bool List::operator < (const Val &other) const
{
    // TODO: Not yet implemented
    return false;
}

bool List::operator >(const Val &other) const
{
    // TODO: Not yet implemented
    return false;
}


List::auto_ptr_t List::clone() const
{
    return auto_ptr_t(new List(*this));
}
