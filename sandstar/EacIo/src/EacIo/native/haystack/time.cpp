//
// Copyright (c) 2015, J2 Innovations
// Copyright (c) 2012 Brian Frank
// Licensed under the Academic Free License version 3.0
// History:
//   06 Jun 2011  Brian Frank  Creation
//   19 Aug 2014  Radu Racariu<radur@2inn.com> Ported to C++
//
#include "time.hpp"
#include <sstream>
#include <ctime>
#include <vector>
#include <stdexcept>
#include <boost/lexical_cast.hpp>


////////////////////////////////////////////////
// Time
////////////////////////////////////////////////
using namespace haystack;

std::vector<int> Time::parseTimeString(const std::string& s) {

	int hour, min, sec, ms = 0;
    try { hour = boost::lexical_cast<int>(s.substr(0, 2)); }
	catch (std::exception&) { throw std::invalid_argument("Invalid hours: " + s); }
        
    if ( s[2] != ':' ) throw std::invalid_argument(s);

	try { min = boost::lexical_cast<int>(s.substr(3, 2)); }
    catch (std::exception&) { throw std::invalid_argument("Invalid minutes: " + s); }

	if ( s[5] != ':' ) throw std::invalid_argument(s);

	try { sec = boost::lexical_cast<int>(s.substr(6, 2)); }
    catch (std::exception&) { throw std::invalid_argument("invalid seconds: " + s); }

	if (s.size() == time_mask_len) return { hour, min, sec, ms };

	if (s[8] != '.') throw std::invalid_argument(s);

    size_t pos = 9;
    int places = 0;

    while (pos < s.size()) {
      ms = (ms * 10) + (s[pos] - '0');
      ++pos;
      ++places;
    }

    switch (places) {
      case 1: ms *= 100; break;
      case 2: ms *= 10; break;
      case 3: break;
      default: throw std::invalid_argument(s);
    }

	return { hour, min, sec, ms };
}

////////////////////////////////////////////////
// to zinc
////////////////////////////////////////////////

// Encode as "hh:mm:ss.FFF"
const std::string Time::to_zinc() const
{
    std::stringstream os;
    if (hour < 10) os << '0'; os << hour << ':';
    if (minutes < 10) os << '0'; os << minutes << ':';
    if (sec < 10) os << '0'; os << sec;
    if (ms != 0)
    {
        os << '.';
        if (ms < 10) os << '0';
        if (ms < 100) os << '0';
        os << ms;
    }

    return os.str();
}

const Time& Time::MIDNIGHT = *new Time(0, 0, 0);

////////////////////////////////////////////////
// Equal
////////////////////////////////////////////////
bool Time::operator ==(const Time &other) const
{
    return (hour == other.hour && minutes == other.minutes && sec == other.sec && ms == other.ms);
}

bool Time::operator==(const Val &other) const
{
    if (type() != other.type())
        return false;
    return *this == static_cast<const Time&>(other);
}

////////////////////////////////////////////////
// Comparators
////////////////////////////////////////////////
bool Time::operator <(const Val &other) const
{
    return type() == other.type() && compareTo(((Time&)other)) < 0;
}

bool Time::operator >(const Val &other) const
{
    return type() == other.type() && compareTo(((Time&)other)) > 0;
}

Time::auto_ptr_t Time::clone() const
{
    return auto_ptr_t(new Time(*this));
}

int Time::compareTo(const Time &other) const
{
    if (hour < other.hour) return -1; else if (hour > other.hour) return 1;
    if (minutes < other.minutes)   return -1; else if (minutes > other.minutes)   return 1;
    if (sec < other.sec)   return -1; else if (sec > other.sec)   return 1;
    if (ms < other.ms)     return -1; else if (ms > other.ms)     return 1;

    return 0;
}
