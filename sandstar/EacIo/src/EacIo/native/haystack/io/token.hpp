#pragma once
#include <string>
#include <boost/noncopyable.hpp>
#include <memory>



namespace haystack {

	class Token : boost::noncopyable {
	public:

		typedef std::shared_ptr<Token> ptr_t;
		
		static const ptr_t eof;
		static const ptr_t id;
		static const ptr_t num;
		static const ptr_t str;
		static const ptr_t ref;
		static const ptr_t uri;
		static const ptr_t date;
		static const ptr_t time;
		static const ptr_t dateTime;

		static const ptr_t colon;
		static const ptr_t comma;
		static const ptr_t semicolon;
		static const ptr_t minus;
		static const ptr_t eq;
		static const ptr_t notEq;
		static const ptr_t lt;
		static const ptr_t lt2;
		static const ptr_t ltEq;
		static const ptr_t gt;
		static const ptr_t gt2;
		static const ptr_t gtEq;
		static const ptr_t lbracket;
		static const ptr_t rbracket;
		static const ptr_t lbrace;
		static const ptr_t rbrace;
		static const ptr_t lparen;
		static const ptr_t rparen;
		static const ptr_t arrow;
		static const ptr_t slash;
		static const ptr_t assign;
		static const ptr_t bang;
		static const ptr_t nl;
		const std::string symbol;
		const bool literal;


		Token( const char* s, bool l = false );
		Token( std::string& s, bool l = false );
		bool operator == (const Token &b) const;
	};

	

}