#include "token.hpp"
#include <string.h>

using namespace haystack;

const Token::ptr_t Token::eof = Token::ptr_t(new Token("eof"));
const Token::ptr_t Token::id = Token::ptr_t(new Token("identifier"));
const Token::ptr_t Token::num = Token::ptr_t(new Token("Number", true));
const Token::ptr_t Token::str = Token::ptr_t(new Token("Str", true));
const Token::ptr_t Token::ref = Token::ptr_t(new Token("Ref", true));
const Token::ptr_t Token::uri = Token::ptr_t(new Token("Uri", true));
const Token::ptr_t Token::date = Token::ptr_t(new Token("Date", true));
const Token::ptr_t Token::time = Token::ptr_t(new Token("Time", true));
const Token::ptr_t Token::dateTime = Token::ptr_t(new Token("DateTime", true));
const Token::ptr_t Token::colon = Token::ptr_t(new Token(":"));
const Token::ptr_t Token::comma = Token::ptr_t(new Token(","));
const Token::ptr_t Token::semicolon = Token::ptr_t(new Token(";"));
const Token::ptr_t Token::minus = Token::ptr_t(new Token("-"));
const Token::ptr_t Token::eq = Token::ptr_t(new Token("=="));
const Token::ptr_t Token::notEq = Token::ptr_t(new Token("!="));
const Token::ptr_t Token::lt = Token::ptr_t(new Token("<"));
const Token::ptr_t Token::lt2 = Token::ptr_t(new Token("<<"));
const Token::ptr_t Token::ltEq = Token::ptr_t(new Token("<="));
const Token::ptr_t Token::gt = Token::ptr_t(new Token(">"));
const Token::ptr_t Token::gt2 = Token::ptr_t(new Token(">>"));
const Token::ptr_t Token::gtEq = Token::ptr_t(new Token(">="));
const Token::ptr_t Token::lbracket = Token::ptr_t(new Token("["));
const Token::ptr_t Token::rbracket = Token::ptr_t(new Token("]"));
const Token::ptr_t Token::lbrace = Token::ptr_t(new Token("{"));
const Token::ptr_t Token::rbrace = Token::ptr_t(new Token("}"));
const Token::ptr_t Token::lparen = Token::ptr_t(new Token("("));
const Token::ptr_t Token::rparen = Token::ptr_t(new Token(")"));
const Token::ptr_t Token::arrow = Token::ptr_t(new Token("->"));
const Token::ptr_t Token::slash = Token::ptr_t(new Token("/"));
const Token::ptr_t Token::assign = Token::ptr_t(new Token("="));
const Token::ptr_t Token::bang = Token::ptr_t(new Token("!"));
const Token::ptr_t Token::nl = Token::ptr_t(new Token("newline"));

Token::Token( const char* s, bool l ) : symbol(s), literal(l) {}
Token::Token(std::string& s, bool l) : symbol(s), literal(l) {}
//Token::Token(const Token& t) : symbol(t.symbol), literal(t.literal) {}
bool Token::operator == (const Token &b) const {
	return this->symbol == b.symbol && literal == b.literal;
}

