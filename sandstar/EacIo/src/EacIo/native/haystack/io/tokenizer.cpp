#include "tokenizer.hpp"
#include "ref.hpp"
#include "num.hpp"
#include "date.hpp"
#include "datetime.hpp"
#include "time.hpp"
#include "uri.hpp"
#include <iostream>
#include <sstream>
#include <locale>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/format.hpp>
#include <ios>

using namespace haystack;

Tokenizer::Tokenizer(std::istream& is) :
	tok(Token::eof),
	m_is(is)
{
    consume();
    consume();
}

//Tokenizer::Tokenizer(const std::string& s) :
//	tok(Token::eof),
//	m_is(std::auto_ptr<std::istream>(new std::istringstream(s)))
//{
//	consume();
//    consume();
//}
void Tokenizer::consume() {

    if ( !m_is.good() ) {
      m_peek = m_eof;
    }

	m_cur = m_peek;
	m_peek = m_is.get();
}

void Tokenizer::consume(int expected) {
    if (m_cur != expected) throw std::runtime_error("Expecting " + (char)expected);
    consume();
}

Token::ptr_t Tokenizer::next() {
    // reset
    val.reset();

    // skip non-meaningful whitespace and comments
    int start_line = line_num;

    while (true) {
      // treat space, tab, non-breaking space as whitespace
      if (m_cur == ' ' || m_cur == '\t' || m_cur == 0xa0) { consume(); continue; }

      // comments
      if (m_cur == '/') {
        if (m_peek == '/') { skipCommentsSL(); continue; }
        if (m_peek == '*') { skipCommentsML(); continue; }
      }

      break;
    }

    // newlines
    if (m_cur == '\n' || m_cur == '\r') {
      if (m_cur == '\r' && m_peek == '\n') consume('\r');
      consume();
      ++line_num;
      return tok = Token::nl;
    }

    // handle various starting chars
    if (is_id_start(m_cur)) return tok = read_id();
    if (m_cur == '"')     return tok = read_str();
    if (m_cur == '@')     return tok = read_ref();
    if (isdigit(m_cur))   return tok = read_num();
    if (m_cur == '`')     return tok = read_uri();
    if (m_cur == '-' && isdigit(m_peek)) return tok = read_num();

    return tok = read_symbol();
}

void Tokenizer::skipCommentsSL() {
    consume('/');
    consume('/');
    while (true) {
      if (m_cur == '\n' || m_cur == m_eof) break;
      consume();
    }
}

void Tokenizer::skipCommentsML() {
    consume('/');
    consume('*');
    int depth = 1;

    while (true) {
      if (m_cur == '*' && m_peek == '/') { consume('*'); consume('/'); depth--; if (depth <= 0) break; }
      if (m_cur == '/' && m_peek == '*') { consume('/'); consume('*'); depth++; continue; }
      if (m_cur == '\n') ++line_num;
      if (m_cur == m_eof) throw std::runtime_error("Multi-line comment not closed");// at " + line_num + " line");
      consume();
    }
  }

bool Tokenizer::is_id_start(int32_t cur) {
	if ('a' <= cur && cur <= 'z') return true;
    if ('A' <= cur && cur <= 'Z') return true;
    return false;
}

bool Tokenizer::is_id_part(int32_t cur) {
    if (is_id_start(cur)) return true;
    if (isdigit(cur)) return true;
    if (cur == '_') return true;
    return false;
}

Token::ptr_t Tokenizer::read_id() {
    std::stringstream s;
    while (is_id_part(m_cur)) { s << (char)m_cur; consume(); }
    this->val.reset( new Ref(s.str()) );
	return Token::id;
}

inline void utf8_encode(const int32_t code_point, std::stringstream& ss)
{
    if (code_point <= 0x7F)
    {
        ss << (char)code_point;
    }
    else if (code_point >= 0x80 && code_point <= 0x7FF)
    {
        ss << (char)((code_point >> 6) | 0xC0)
            << (char)((code_point & 0x3F) | 0x80);
    }
    else if (code_point >= 0x800 && code_point <= 0xFFFF)
    {
        ss << (char)((code_point >> 12) | 0xE0)
            << (char)(((code_point >> 6) & 0x3F) | 0x80)
            << (char)((code_point & 0x3F) | 0x80);
    }
    else if (code_point >= 0x10000 && code_point <= 0x1FFFFF)
    {
        ss << (char)((code_point >> 18) | 0xF0)
            << (char)(((code_point >> 12) & 0x3F) | 0x80)
            << (char)(((code_point >> 6) & 0x3F) | 0x80)
            << (char)((code_point & 0x3F) | 0x80);
    }

}

Token::ptr_t Tokenizer::read_str() {
    consume('"');
    std::stringstream s;

    while (true) {
      if (m_cur == m_eof) throw std::runtime_error("Unexpected end of str literal");
      if (m_cur == '"') { consume('"'); break; }
      if (m_cur == '\\') { 
		  utf8_encode(read_esc_char(), s);
		  continue;
	  }
      s << (char)m_cur;
      consume();
    }

    this->val.reset( new Str(s.str()) );
    return Token::str;
}

Token::ptr_t Tokenizer::read_ref() {
    consume('@'); // opening @
    std::stringstream s;

//    if (m_cur < 0) throw std::runtime_error("Unexpected end of ref literal");

    while (Ref::is_id_char(m_cur)) {
//        if (m_cur < 0) throw std::runtime_error("Unexpected end of ref literal");
//        if (m_cur == '\n' || m_cur == '\r') throw std::runtime_error("Unexpected newline in ref literal");
        s << (char)m_cur;
        consume();
    }

	this->val.reset( new Ref(s.str()) );
    return Token::ref;
}

Token::ptr_t Tokenizer::read_num() {
    // parse numeric part
    
	bool is_hex = m_cur == '0' && m_peek == 'x';

	if ( is_hex ) {
		consume('0');
		consume('x');
		std::stringstream s;

		while( Tokenizer::is_hex(m_cur) || m_cur == '_' ) {
			if ( m_cur == '_') { consume(); continue; }
			s << (char) m_cur;
			consume();
		}
		long long n;
		s >> std::hex >> n;
		this->val.reset( new Num(n) );
		return Token::num;
	}

	std::stringstream s;
    s << (char)m_cur;
    consume();

	int colons = 0;
    int dashes = 0;
    int unitIndex = 0;
    bool exp = false;

    while (true)
    {
		if (!isdigit(m_cur)) {
		  if (exp && (m_cur == '+' || m_cur == '-')) { }
		  else if (m_cur == '-') { ++dashes; }
		  else if (m_cur == ':' && isdigit(m_peek)) { ++colons; }
		  else if ((exp || colons >= 1) && m_cur == '+') { }
		  else if (m_cur == '.') { if (!isdigit(m_peek)) break; }
		  else if ((m_cur == 'e' || m_cur == 'E') && (m_peek == '-' || m_peek == '+' || isdigit(m_peek))) { exp = true; }
		  else if (isalpha(m_cur) || m_cur == '%' || m_cur == '$' || m_cur == '/' || m_cur > 128) { if (unitIndex == 0) unitIndex = s.tellp(); }
		  else if (m_cur == '_') { if (unitIndex == 0 && isdigit(m_peek)) { consume(); continue; }  else { if (unitIndex == 0) unitIndex = s.tellp(); } }
		  else { break; }
		}
		s << (char)m_cur;
		consume();
    }

	if (dashes == 2 && colons == 0) return date(s.str());
    if (dashes == 0 && colons >= 1) return this->time(s.str(), colons == 1);
    if (dashes >= 2) return date_time(s);
    return number(s.str(), unitIndex);
}

Token::ptr_t Tokenizer::read_symbol() {
	int c = m_cur;
    consume();
	
    switch(c)
    {
      case ',':
        return Token::comma;
      case ':':
        return Token::colon;
      case ';':
        return Token::semicolon;
      case '[':
        return Token::lbracket;
      case ']':
        return Token::rbracket;
      case '{':
        return Token::lbrace;
      case '}':
        return Token::rbrace;
      case '(':
        return Token::lparen;
      case ')':
        return Token::rparen;
      case '<':
        if (m_cur == '<') { consume('<'); return Token::lt2; }
        if (m_cur == '=') { consume('='); return Token::ltEq; }
        return Token::lt;
      case '>':
        if (m_cur == '>') { consume('>'); return Token::gt2; }
        if (m_cur == '=') { consume('='); return Token::gtEq; }
        return Token::gt;
      case '-':
        if (m_cur == '>') { consume('>'); return Token::arrow; }
        return Token::minus;
      case '=':
        if (m_cur == '=') { consume('='); return Token::eq; }
        return Token::assign;
      case '!':
        if (m_cur == '=') { consume('='); return Token::notEq; }
        return Token::bang;
      case '/':
        return Token::slash;
	}
    if (c < 0) return Token::eof;
    throw parse_error(boost::str(boost::format("Unexpected char '%1$c %1$0X' for start of value") % m_cur));
}

bool Tokenizer::is_hex(int32_t cur ) {
	cur = tolower(cur);
    return ('a' <= cur && cur <= 'f') || isdigit(cur);
}

Token::ptr_t Tokenizer::date(const std::string& s) {
	this->val.reset( new Date(s) );
	return Token::date;
}

Token::ptr_t Tokenizer::time(std::string s, bool addSeconds) {

	if (s[1] == ':') s.insert(0, "0");
	if (addSeconds) s.append(":00");
	this->val.reset( new Time(s) );
	return Token::time;
}

Token::ptr_t Tokenizer::date_time(std::stringstream& s) {
	// xxx timezone
	bool zUtc = false;
	
    if (m_cur != ' ' || !isupper(m_peek)) {
      if (boost::algorithm::ends_with(s.str(), "Z")) zUtc = true;
      else throw std::invalid_argument("Expecting timezone");

    } else {
      consume();
      s << " ";
      while (is_id_part(m_cur)) { s << (char)m_cur; consume(); }

      // handle GMT+xx or GMT-xx
      if ((m_cur == '+' || m_cur == '-') && boost::algorithm::ends_with(s.str(), "GMT")) {
        s << (char)m_cur; consume();
        while (isdigit(m_cur)) { s << (char)m_cur; consume(); }
      }
    }

    
	this->val.reset( DateTime::fromString(s.str()).clone().release() );
	return Token::dateTime;
}

Token::ptr_t Tokenizer::number(const std::string& s, int unitIndex) {
	try {
      if (unitIndex == 0) {
        this->val.reset( new Num(boost::lexical_cast<double>(s)) );
      } else {
        std::string doubleStr = s.substr(0, unitIndex);
        std::string unitStr   = s.substr(unitIndex);
        this->val.reset( new Num(boost::lexical_cast<double>(doubleStr), unitStr));
      }
    } catch (std::exception& e) {
      throw parse_error("Invalid Number literal: " + s);
    }

    return Token::num;
}

Token::ptr_t Tokenizer::read_uri() {
    consume('`'); // opening backtick
    std::stringstream s;

    for (;;) {
        if (m_cur < 0) throw parse_error("Unexpected end of uri literal");
        if (m_cur == '\n') throw parse_error("Unexpected newline in uri literal");
        if (m_cur == '`') { consume('`'); break; }
        if (m_cur == '\\')
        {
            switch (m_peek)
            {
            case ':': case '/': case '?': case '#':
            case '[': case ']': case '@': case '\\':
            case '&': case '=': case ';':
                s << (char)m_cur;
                s << (char)m_peek;
                consume();
                consume();
                break;
//            case '`':
//                s << '`';
//                consume();
//                consume();
//                break;
            default:
//                if (m_peek == 'u' || m_peek == '\\')
				utf8_encode(read_esc_char(), s);
//                else throw std::runtime_error("Invalid URI escape sequence \\" + (char)m_cur);
                break;
            }
        }
        else
        {
            s << (char)m_cur;
            consume();
        }
    }
//    consume(); // closing backtick
	this->val.reset( new Uri(s.str()) );
    return Token::uri;
  }


int32_t Tokenizer::read_esc_char() {
    consume('\\');  // back slash

    // check basics
    switch (m_cur) {
		case 'b':   consume(); return '\b';
		case 'f':   consume(); return '\f';
		case 'n':   consume(); return '\n';
		case 'r':   consume(); return '\r';
		case 't':   consume(); return '\t';
		case '"':   consume(); return '"';
		case '$':   consume(); return '$';
		case '\'': consume(); return '\'';
		case '`':  consume(); return '`';
		case '\\':  consume(); return '\\';
    }

    // check for uxxxx
    if (m_cur == 'u') {
        consume('u');
		std::stringstream ss;
		ss << (char)m_cur; consume();
		ss << (char)m_cur; consume();
		ss << (char)m_cur; consume();
		ss << (char)m_cur; consume();
        int32_t res;
        ss >> std::hex >> res;
		return res;
    }

    throw std::runtime_error("Invalid escape sequence: \\");
}
