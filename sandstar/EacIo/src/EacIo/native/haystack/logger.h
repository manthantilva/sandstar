/*************************************************
 * Wrapper class for logger. This logger can write log to syslog OR stderr OR
 *both.
 * Construct is private so force to use getInstance function which implement
 *singletone pattern.
 * Default log to syslog only.This can be changed by calling setType with
 *appropriate LoggerType.
 * Default log level is NONE. This can be changes by calling setLevel with
 *appropriate LogLevel.
 * Provide macro for traceing code during development.
 *************************************************/
// file : logger.h
// author : Manthan R. Tilva
// date : 11-06-2018
// History : First implimentation
//          : Adding support for filters 14-06-2018

#ifndef haystack_Logger_h_
#define haystack_Logger_h_
#include <cstdarg>
#include <ctime>

#include <functional>
#include <map>
#include <memory>
#include <syslog.h>
#include <unordered_map>
#include <vector>

namespace haystack {

class Logger {
  Logger() {
    m_logFunc =
        std::bind(&Logger::logToSyslogStderr, *this, std::placeholders::_1,
                  std::placeholders::_2, std::placeholders::_3);
  };
  std::function<void(int, const char *, va_list)> m_logFunc;
  void logToStderrOnly(int level, const char *format, va_list ap) {
    std::time_t t = std::time(nullptr);
    char time_buf[100];
    std::strftime(time_buf, sizeof time_buf, "[%D %T]: ", std::gmtime(&t));
    std::string msg = std::string(time_buf).append(format);
    if (msg.back() != '\n')
      msg.push_back('\n');
    vfprintf(stderr, msg.c_str(), ap);
  }
  void logToSyslogOnly(int level, const char *format, va_list ap) {
    vsyslog(level, format, ap);
  }
  void logToSyslogStderr(int level, const char *format, va_list ap) {
    logToStderrOnly(level, format, ap);
    logToSyslogOnly(level, format, ap);
  }

public:
  enum class LoggerFilter {
    ALL,
    ENGINE,
    LOGIN,
    USERS,
    HAYSTACKOPS,
    POINTWRITE,
    IPCS,
    STARTUP,
    UNNAMED
  };
  enum LoggerType { SYSLOG_ONLY, STDERR_ONLY, SYSLOG_STDERR, UNKNOWN };
  enum LogLevel { NONE, EMERG, ALERT, CRIT, ERR, WARNING, NOTICE, INFO, DEBUG };
  ~Logger() {}
  void debug(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::DEBUG) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_DEBUG, std::string("["+m_strFilterName+"]: ").append("[DEBUG]: ").append(format).c_str(), ap);
      va_end(ap);
    }
  }
  void info(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::INFO) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_INFO, std::string("["+m_strFilterName+"]: ").append("[INFO]: ").append(format).c_str(), ap);
      va_end(ap);
    }
  }
  void notice(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::NOTICE) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_NOTICE, std::string("["+m_strFilterName+"]: ").append("[NOTICE]: ").append(format).c_str(),
                ap);
      va_end(ap);
    }
  }
  void warning(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::WARNING) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_WARNING, std::string("["+m_strFilterName+"]: ").append("[WARNING]: ").append(format).c_str(),
                ap);
      va_end(ap);
    }
  }
  void err(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::ERR) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_ERR, std::string("["+m_strFilterName+"]: ").append("[ERR]: ").append(format).c_str(), ap);
      va_end(ap);
    }
  }
  void crit(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::CRIT) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_CRIT, std::string("["+m_strFilterName+"]: ").append("[CRIT]: ").append(format).c_str(), ap);
      va_end(ap);
    }
  }
  void alert(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::ALERT) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_ALERT, std::string("["+m_strFilterName+"]: ").append("[ALERT]: ").append(format).c_str(), ap);
      va_end(ap);
    }
  }
  void emerg(const char *format, ...) {
    if (m_oLogLevel >= LogLevel::EMERG) {
      va_list ap;
      va_start(ap, format);
      m_logFunc(LOG_EMERG, std::string("["+m_strFilterName+"]: ").append("[EMERG]: ").append(format).c_str(), ap);
      va_end(ap);
    }
  }
  static void setLoggerType(const LoggerType &type);
  static Logger &getInstance() {
    static Logger *m_pLogger{nullptr};
    if (!m_pLogger) {
      m_pLogger = new Logger();
    }
    return *m_pLogger;
  }
  static std::unique_ptr<Logger> &getInstance(const LoggerFilter &filter);
  static void enableLogFilter(const LoggerFilter &filter,
                              const LogLevel &level);
  static LoggerFilter filterStringToClass(const std::string &filter);
  static LogLevel levelStringToClass(const std::string &level);

private:
  void setLoggerFilter(LoggerFilter filter) {
    m_oFilter = filter;
    m_strFilterName = m_oFilterName[filter];
  }
  void setLevel(const LogLevel &level) { m_oLogLevel = level; }
  void setTypeInternal(const LoggerType &type) {
    if (m_oLoggerType == LoggerType::UNKNOWN) {
      if (type == LoggerType::SYSLOG_ONLY) {
        m_logFunc =
            std::bind(&Logger::logToSyslogOnly, *this, std::placeholders::_1,
                      std::placeholders::_2, std::placeholders::_3);
      } else if (type == LoggerType::SYSLOG_STDERR) {
        m_logFunc =
            std::bind(&Logger::logToSyslogStderr, *this, std::placeholders::_1,
                      std::placeholders::_2, std::placeholders::_3);
      } else if (type == LoggerType::STDERR_ONLY) {
        m_logFunc =
            std::bind(&Logger::logToStderrOnly, *this, std::placeholders::_1,
                      std::placeholders::_2, std::placeholders::_3);
      }
      m_oLoggerType = type;
      openlog(nullptr, 0, 0);
    } else {
      this->err("Logger type already set.");
    }
  }

private:
  LoggerType m_oLoggerType{LoggerType::UNKNOWN};
  LogLevel m_oLogLevel{LogLevel::NONE};
  LoggerFilter m_oFilter{LoggerFilter::UNNAMED};
  std::string m_strFilterName{"UNNAMED"};
  static std::map<LoggerFilter, std::string> m_oFilterName;
  static std::vector<std::string> m_oLevelList;
  static LogLevel m_oLogLevelStatic;//Used only if filter is ALL {LogLevel::NONE};
  static LoggerType m_oLogTypeStatic;
  struct FilterHasher {
    std::size_t operator()(const Logger::LoggerFilter &k) const {
      using std::hash;
      return (hash<int>()(static_cast<int>(k)) << 1);
    }
  };

  static std::unordered_map<Logger::LoggerFilter, std::unique_ptr<Logger>,
                            FilterHasher>
      m_oLoggerMap;
};
} // namespace haystack

#include "loggerMacro.h"

#endif //  haystack_Logger_h_