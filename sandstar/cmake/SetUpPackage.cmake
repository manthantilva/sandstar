
install(FILES sandstar_service/sandstar.service DESTINATION etc/init)
install(FILES opt/skyspark/var/security/AllowedCorsUrl.config DESTINATION opt/skyspark/var/security/AllowedCorsUrl.config)
install(FILES EacIo/apps/platUnix.sax DESTINATION share)
install(FILES EacIo/database.zinc DESTINATION share)



set(CPACK_GENERATOR "DEB" CACHE STRING "" FORCE)
set(CPACK_PACKAGE_CONTACT "info@ankalabs.com" CACHE STRING "" FORCE)
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Anka Labs Team" CACHE STRING "" FORCE)
set(CPACK_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX} CACHE STRING "" FORCE)
set(CPACK_SET_DESTDIR ON CACHE BOOL "" FORCE)
set(CPACK_PACKAGE_VERSION_MAJOR ${sandstar_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${sandstar_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${sandstar_VERSION_PATCH})

# message(${CMAKE_SYSTEM_NAME})
if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
set(CPACK_PACKAGE_FILE_NAME "sandstar-${sandstar_VERSION}-${CMAKE_SYSTEM_NAME}-debug")
else(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
set(CPACK_PACKAGE_FILE_NAME "sandstar-${sandstar_VERSION}-${CMAKE_SYSTEM_NAME}")
endif(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
include(CPack)
