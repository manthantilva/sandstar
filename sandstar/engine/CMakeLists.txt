

add_executable(engine${EXECUTABLE_POSTFIX} src/anio.c src/channel.c src/csv.c src/engine.c src/gpio.c src/i2cio.c src/io.c src/notify.c src/poll.c src/pwmio.c src/table.c src/value.c src/watch.c src/zinc.c)

target_include_directories(engine${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)
target_link_libraries(engine${EXECUTABLE_POSTFIX} pthread)

add_executable(watch${EXECUTABLE_POSTFIX} tools/watch.c)
target_include_directories(watch${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)

add_executable(notify${EXECUTABLE_POSTFIX} tools/notify.c)
target_include_directories(notify${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)

add_executable(read${EXECUTABLE_POSTFIX} tools/read.c)
target_include_directories(read${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)

add_executable(write${EXECUTABLE_POSTFIX} tools/write.c)
target_include_directories(write${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)

add_executable(convert${EXECUTABLE_POSTFIX} tools/convert.c)
target_include_directories(convert${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)

add_executable(pwmscan${EXECUTABLE_POSTFIX} tools/pwmscan.c)
target_include_directories(pwmscan${EXECUTABLE_POSTFIX} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/src/)

install(TARGETS engine${EXECUTABLE_POSTFIX} watch${EXECUTABLE_POSTFIX} notify${EXECUTABLE_POSTFIX} read${EXECUTABLE_POSTFIX} write${EXECUTABLE_POSTFIX} convert${EXECUTABLE_POSTFIX} pwmscan${EXECUTABLE_POSTFIX} RUNTIME DESTINATION bin)
