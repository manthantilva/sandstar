// read.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "engine.h"

// Definitions

#define CLIENT_KEY		0x45583032

// Local functions

static int display_error(char *sFormat,...);
static int send_message(int hQueue,int nMessage,ENGINE_CHANNEL channel,ENGINE_VALUE *value);

static void terminate(int sig);

// Local data

static volatile sig_atomic_t g_flag=0;

// Public functions

// main
// Called at program startup

int main(int argc,char *argv[])
{
	static char *sStatus[]={"ok","unknown","stale","disable","fault","down"};

	// check for args
	if(argc<=1)
	{
		printf("read: usage: read [channel...]\n");
		return 0;
	}

	// catch ctrl+c
	signal(SIGINT,terminate);

	// engine queue found?
	int hEngine=msgget(ENGINE_MESSAGE_KEY,0666);

	if(hEngine<0)
	{
		display_error("engine not started");
		return -1;
    }

	// create queue?
	int hQueue=msgget(CLIENT_KEY,IPC_CREAT|0666);

	if(hQueue<0)
	{
		display_error("failed to create message queue");
		return -1;
    }

	// send read messages
	int nTotal=0;
	int nAcks=0;

	int n;

	for(n=1;n<argc;n++)
	{
		// get channel and send message
		ENGINE_CHANNEL channel=strtol(argv[n],NULL,10);
		send_message(hEngine,ENGINE_MESSAGE_READ,channel,NULL);

		nTotal++;
	}

	// output headings
	printf("\nchannel status  raw     value\n");
	printf("------- ------- ------- -------\n");

	// while not ctrl+c
	while(g_flag==0)
	{
		// read messages
		ENGINE_MESSAGE msg;

		int err=msgrcv(hQueue,&msg,ENGINE_MESSAGE_SIZE,0,0);

		if(err>=0)
		{
			// what message?
			switch(msg.nMessage)
			{
				case ENGINE_MESSAGE_READACK:
					// read ack message
					printf("%-7d %-7.7s %-7g %g\n",
						msg.channel,
						sStatus[(int) msg.value.status],
						msg.value.raw,
						msg.value.cur);

					if(++nAcks>=nTotal) g_flag=1;
					break;

				default:
					// unknown message
					display_error("received unknown message (0x%-2.2lX)",msg.nMessage);
					break;
			}
		}

		// err!=ENOMSG ...
	}

	printf("\n");

	// kill queue
	if(msgctl(hQueue,IPC_RMID,0)<0)
	{
		display_error("failed to destroy message queue");
		return -1;
	}
	return 0;
}

// display_error
// Call to display engine error

static int display_error(char *sFormat,...)
{
    va_list args;

	// output error
	printf("read: error: ");

	va_start(args,sFormat);
	vprintf(sFormat,args);
	va_end(args);

	printf("\n");

	return -1;
}

// send_message
// Call to send engine message

static int send_message(int hQueue,int nMessage,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	ENGINE_MESSAGE msg;
	memset(&msg,0,sizeof(ENGINE_MESSAGE));

	msg.nMessage=nMessage;
	msg.channel=channel;
	msg.sender=CLIENT_KEY;

	if(value!=NULL) msg.value=*value;

	int err=msgsnd(hQueue,&msg,ENGINE_MESSAGE_SIZE,0);
	if(err<0) display_error("failed to send message");

	return err;
}

// terminate
// Called when ctrl+c pressed

static void terminate(int sig)
{
	// signal termination
	g_flag=1;
}
