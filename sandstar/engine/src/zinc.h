// zinc.h

#ifndef __ZINC_H__
#define __ZINC_H__

// Definitions

#define ZINC_VERSIONMAJOR	3
#define ZINC_VERSIONMINOR	0

#define ZINC_NULL			"N"
#define ZINC_MARKER			"M"
#define ZINC_TRUE			"T"
#define ZINC_FALSE			"F"

// Zinc grid

struct _ZINC
{
	char *pData;
	char *pParse;

	int nVersionMajor;
	int nVersionMinor;

	int nRows;
	int nColumns;

	char **sTags;
	char **sGrid;
};

typedef struct _ZINC ZINC;

// Public functions

int zinc_init(ZINC *zinc);
int zinc_exit(ZINC *zinc);

int zinc_load(ZINC *zinc,char *sFile);

int zinc_tag(ZINC *zinc,char *sTag);
int zinc_grid(ZINC *zinc,int nRow,int nColumn,char **sData);

int zinc_null(ZINC *zinc,int nRow,char *sTag,int nDefault);
int zinc_marker(ZINC *zinc,int nRow,char *sTag,int nDefault);

int zinc_bool(ZINC *zinc,int nRow,char *sTag,int nDefault);
int zinc_integer(ZINC *zinc,int nRow,char *sTag,int nDefault);
double zinc_number(ZINC *zinc,int nRow,char *sTag,double fDefault);
char *zinc_string(ZINC *zinc,int nRow,char *sTag,char *sDefault);

int zinc_report(ZINC *zinc,char *sFile);
int zinc_report_tags(ZINC *zinc);
int zinc_report_grid(ZINC *zinc,char *sTags);

#endif // _ZINC_H__
