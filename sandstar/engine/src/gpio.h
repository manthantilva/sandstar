// gpio.h

#ifndef __GPIO_H__
#define __GPIO_H__

// Direction type

enum _GPIO_DIRECTION
{
	GP_IN,
	GP_OUT,
	GP_HIGH,
	GP_LOW
};

typedef enum _GPIO_DIRECTION GPIO_DIRECTION;

// Edge type

enum _GPIO_EDGE
{
	GP_NONE,
	GP_RISING,
	GP_FALLING,
	GP_BOTH
};

typedef enum _GPIO_EDGE GPIO_EDGE;

// Active low type

enum _GPIO_ACTIVELOW
{
	GP_DISABLE,
	GP_ENABLE
};

typedef enum _GPIO_ACTIVELOW GPIO_ACTIVELOW;

// Typedefs

typedef unsigned int GPIO_ADDRESS;
typedef unsigned int GPIO_VALUE;

// Public functions

int gpio_exists(GPIO_ADDRESS address);

int gpio_export(GPIO_ADDRESS address);
int gpio_unexport(GPIO_ADDRESS address);

int gpio_get_direction(GPIO_ADDRESS address,GPIO_DIRECTION *value);
int gpio_set_direction(GPIO_ADDRESS address,GPIO_DIRECTION value);

int gpio_get_edge(GPIO_ADDRESS address,GPIO_EDGE *value);
int gpio_set_edge(GPIO_ADDRESS address,GPIO_EDGE value);

int gpio_get_activelow(GPIO_ADDRESS address,GPIO_ACTIVELOW *value);
int gpio_set_activelow(GPIO_ADDRESS address,GPIO_ACTIVELOW value);

int gpio_get_value(GPIO_ADDRESS address,GPIO_VALUE *value);
int gpio_set_value(GPIO_ADDRESS address,GPIO_VALUE value);

#endif // __GPIO_H__
