// value.h

#ifndef __VALUE_H__
#define __VALUE_H__

// Convertion flags

#define VALUE_CONV_USELOW		0x0001
#define VALUE_CONV_USEHIGH		0x0002

#define VALUE_CONV_USEOFFSET	0x0004
#define VALUE_CONV_USESCALE		0x0008

#define VALUE_CONV_USEMIN		0x0010
#define VALUE_CONV_USEMAX		0x0020

#define VALUE_CONV_ADC			0x0100
#define VALUE_CONV_DAC			0x0200

#define VALUE_CONV_USERANGE		(VALUE_CONV_USELOW|VALUE_CONV_USEHIGH)

// Value convertion

struct _VALUE_CONV
{
	int nTable;

	double low;
	double high;

	double offset;
	double scale;

	double min;
	double max;

	int flags;
};

typedef struct _VALUE_CONV VALUE_CONV;

// Public functions

int value_status(ENGINE_VALUE *value,ENGINE_STATUS status);

int value_raw(ENGINE_VALUE *value,ENGINE_DATA raw);
int value_cur(ENGINE_VALUE *value,ENGINE_DATA cur);

int value_convert(ENGINE_VALUE *value,VALUE_CONV *conv,TABLE *tables);
int value_revert(ENGINE_VALUE *value,VALUE_CONV *conv,TABLE *tables);

int value_cmp(ENGINE_VALUE *value1,ENGINE_VALUE *value2);

#endif // __VALUE_H__
