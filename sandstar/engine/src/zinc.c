// zinc.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include "global.h"
#include "zinc.h"

// Definitions

#define MAX_TAG			32
#define MAX_COLS		10

// Local functions

static int zinc_decode_header(ZINC *zinc);
static int zinc_decode_tags(ZINC *zinc);
static int zinc_decode_rows(ZINC *zinc);

static int zinc_next_line(ZINC *zinc);
static int zinc_skip_blank(ZINC *zinc);

// Public functions

// zinc_init
// Call to initialize zinc grid

int zinc_init(ZINC *zinc)
{
	memset(zinc,0,sizeof(ZINC));

	zinc->nVersionMajor=ZINC_VERSIONMAJOR;
	zinc->nVersionMinor=ZINC_VERSIONMINOR;

	return 0;
}

// zinc_exit
// Call to release zinc grid

int zinc_exit(ZINC *zinc)
{
	// free tags
	if(zinc->sTags!=NULL)
	{
		free(zinc->sTags);
		zinc->sTags=NULL;
	}

	// free grid data
	if(zinc->sGrid!=NULL)
	{
		free(zinc->sGrid);
		zinc->sGrid=NULL;
	}

	// free file data
	if(zinc->pData!=NULL)
	{
		free(zinc->pData);
		zinc->pData=NULL;
	}

	// reset members
	zinc->pParse=NULL;

	zinc->nRows=0;
	zinc->nColumns=0;

	return 0;
}

// zinc_load
// Call to load zinc grid into memory

int zinc_load(ZINC *zinc,char *sFile)
{
	// open the file?
	int hFile=open(sFile,O_RDONLY);

	if(hFile<0)
	{
		engine_error("zinc file not found");
		return -1;
	}

	// get file size
	long nLength=lseek(hFile,0,SEEK_END);
	lseek(hFile,0,SEEK_SET);

	if(nLength<=0)
	{
		close(hFile);

		engine_error("zinc file is empty");
		return -1;
	}

	// allocate file memory
	char *pData=(char *) malloc(nLength+1);

	if(pData==NULL)
	{
		close(hFile);

		engine_error("out of memory loading zinc file");
		return -1;
	}

	// read file into memory
	long nSize=read(hFile,pData,nLength);
	close(hFile);

	if(nSize!=nLength)
	{
		free(pData);

		engine_error("zinc file read error");
		return -1;
	}

	// ready for parsing
	pData[nLength]=0;

	zinc->pData=pData;
	zinc->pParse=pData;

	// parse zinc data
	if(zinc_decode_header(zinc)==0 &&
		zinc_decode_tags(zinc)==0)
		return zinc_decode_rows(zinc);

	return -1;
}

// zinc_tag
// Call to get tag grid column index

int zinc_tag(ZINC *zinc,char *sTag)
{
	int n;

	for(n=0;n<zinc->nColumns;n++)
	{
		// tag matches?
		if(strcmp(zinc->sTags[n],sTag)==0)
			return n;
	}
	return -1;
}

// zinc_grid
// Call to get grid data from coordinates

int zinc_grid(ZINC *zinc,int nRow,int nColumn,char **sData)
{
	// valid row?
	if(nRow<0 || nRow>=zinc->nRows)
	{
		engine_error("grid row out of range");

		*sData="";
		return -1;
	}

	// valid column?
	if(nColumn<0 || nColumn>=zinc->nColumns)
	{
//		engine_error("grid column out of range");

		*sData="";
		return -1;
	}

	// set data pointer
	*sData=zinc->sGrid[(nRow*zinc->nColumns)+nColumn];

	return 0;
}

// zinc_null
// Call to get zinc grid data null

int zinc_null(ZINC *zinc,int nRow,char *sTag,int nDefault)
{
	// get tag column
	int nColumn=zinc_tag(zinc,sTag);

	if(nColumn>=0)
	{
		// get grid cell data
		char *sData;
		zinc_grid(zinc,nRow,nColumn,&sData);

		// has marker set?
		return (*sData==0 || strcmp(sData,ZINC_NULL)==0)?1:0;
	}
	return nDefault;
}

// zinc_marker
// Call to get zinc grid data marker

int zinc_marker(ZINC *zinc,int nRow,char *sTag,int nDefault)
{
	// get tag column
	int nColumn=zinc_tag(zinc,sTag);

	if(nColumn>=0)
	{
		// get grid cell data
		char *sData;
		zinc_grid(zinc,nRow,nColumn,&sData);

		// has marker set?
		return (strcmp(sData,ZINC_MARKER)==0)?1:0;
	}
	return nDefault;
}

// zinc_bool
// Call to get zonc grid data bool

int zinc_bool(ZINC *zinc,int nRow,char *sTag,int nDefault)
{
	// get tag column
	int nColumn=zinc_tag(zinc,sTag);

	if(nColumn>=0)
	{
		// get grid cell data
		char *sData;
		zinc_grid(zinc,nRow,nColumn,&sData);

		// has bool set?
		return (strcmp(sData,ZINC_TRUE)==0)?1:0;
	}
	return nDefault;
}

// zinc_integer
// Call to get zinc grid data integer

int zinc_integer(ZINC *zinc,int nRow,char *sTag,int nDefault)
{
	// get tag column
	int nColumn=zinc_tag(zinc,sTag);

	if(nColumn>=0)
	{
		// get grid cell data
		char *sData;
		zinc_grid(zinc,nRow,nColumn,&sData);

		// convert to int
		return strtol(sData,NULL,10);
	}
	return nDefault;
}

// zinc_number
// Call to get zinc grid data number

double zinc_number(ZINC *zinc,int nRow,char *sTag,double fDefault)
{
	// get tag column
	int nColumn=zinc_tag(zinc,sTag);

	if(nColumn>=0)
	{
		// get grid cell data
		char *sData;
		zinc_grid(zinc,nRow,nColumn,&sData);

		// convert to double
		return strtod(sData,NULL);
	}
	return fDefault;
}

// zinc_string
// Call to get zinc grid data string

char *zinc_string(ZINC *zinc,int nRow,char *sTag,char *sDefault)
{
	// get tag column
	int nColumn=zinc_tag(zinc,sTag);

	if(nColumn>=0)
	{
		// get grid cell data
		char *sData;
		zinc_grid(zinc,nRow,nColumn,&sData);

		// get data pointer
		if(*sData=='\"') sData++;

		char *p=sData;

		while(*p!=0)
		{
			if(*p=='\"') *p=0;
			else p++;
		}
		return sData;
	}
	return sDefault;
}

// zinc_report
// Call to output zinc file report

int zinc_report(ZINC *zinc,char *sFile)
{
	// output file report
	printf("\n    file: %s\n",sFile);
	printf(" version: %d.%d\n",zinc->nVersionMajor,zinc->nVersionMinor);
	printf("    cols: %d\n",zinc->nColumns);
	printf("    rows: %d\n\n",zinc->nRows);

	return 0;
}

// zinc_report_tags
// Call to output zinc file tag list

int zinc_report_tags(ZINC *zinc)
{
	// display tags
	int n;

	for(n=0;n<zinc->nColumns;n++)
	{
		if(n%4==0) printf("\n");
		printf("%-16.16s ",zinc->sTags[n]);
	}

	printf("\n\n");

	return 0;
}

// zinc_report_grid
// Call to output zinc file grid

int zinc_report_grid(ZINC *zinc,char *sTags)
{
	// find tag names
	char sTag[MAX_TAG];

	int nCol[MAX_COLS];
	int nWidth[MAX_COLS];

	int nRows=zinc->nRows;
	int nCols=zinc->nColumns;

	int nTags=0;
	int n;

	for(n=0;nTags<MAX_COLS && n<nCols;n++)
	{
		snprintf(sTag,MAX_TAG,",%s,",zinc->sTags[n]);

		if(strstr(sTags,sTag)!=NULL)
		{
			nCol[nTags]=n;
			nWidth[nTags]=strlen(sTag)-2;

			nTags++;
		}
	}

	// found any?
	if(nTags==0)
	{
		engine_error("no tags found");
		return -1;
	}

	// check data widths
	char sFormat[16];
	char *sData;

	int t,r;

	for(t=0;t<nTags;t++)
	{
		for(r=0;r<nRows;r++)
		{
			zinc_grid(zinc,r,nCol[t],&sData);

			int w=strlen(sData);
			if(w>nWidth[t]) nWidth[t]=w;
		}
	}

	// output headings
	printf("\n");

	for(t=0;t<nTags;t++)
	{
		sprintf(sFormat,"%%-%d.%ds ",nWidth[t],nWidth[t]);
		printf(sFormat,zinc->sTags[nCol[t]]);
	}

	printf("\n");

	for(t=0;t<nTags;t++)
	{
		for(r=0;r<nWidth[t];r++)
			printf("-");

		printf(" ");
	}

	printf("\n");

	for(r=0;r<nRows;r++)
	{
		for(t=0;t<nTags;t++)
		{
			zinc_grid(zinc,r,nCol[t],&sData);

			sprintf(sFormat,"%%-%d.%ds ",nWidth[t],nWidth[t]);
			printf(sFormat,sData);
		}
		printf("\n");
	}

	printf("\n");

	return 0;
}

// Local functions

// zinc_decode_header
// Call to decode zinc header

static int zinc_decode_header(ZINC *zinc)
{
	// skip blank lines
	zinc_skip_blank(zinc);

	// decode version
	int count=sscanf(zinc->pParse,"ver:\"%d.%d\"",&zinc->nVersionMajor,&zinc->nVersionMinor);

	if(count!=2)
	{
		engine_error("zinc header not found");
		return -1;
	}
	return 0;
}

// zinc_decode_tags
// Call to decode zinc tags

static int zinc_decode_tags(ZINC *zinc)
{
	// skip to tag line
	if(zinc_next_line(zinc)<0)
	{
		engine_error("zinc tags not found");
		return -1;
	}

	// skip to data
	char *l=zinc->pParse;
	char *p=l;

	zinc_next_line(zinc);

	// count columns
	char c;

	int nColumns=0;
	int nColumn=0;

	while((c=*p)!=0 && c!='\n' && c!='\r')
	{
		while((c=*p)==' ' || c=='\t') p++;

		if(c!=0 && c!='\n' && c!='\r')
		{
			while((c=*p)!=0 && c!=',' && c!='\n' && c!='\r') p++;
			nColumns++;

			if(c==',') p++;			
		}
	}

	// must have columns
	if(nColumns==0)
	{
		engine_error("no zinc tags found");
		return -1;
	}

	// allocate tags
	char **sTags=(char **) malloc(sizeof(char *)*nColumns);

	if(sTags==NULL)
	{
		engine_error("out of memory allocating zinc tags");
		return -1;
	}

	// read tag names
	p=l;

	while((c=*p)!=0 && c!='\n' && c!='\r')
	{
		// skip whitespace
		while((c=*p)==' ' || c=='\t') p++;

		if(c!=0 && c!='\n' && c!='\r')
		{
			// set tag name
			sTags[nColumn]=p;

			// skip to next name
			while((c=*p)!=0 && c!=',' && c!='\n' && c!='\r') p++;
			nColumn++;

			*p=0;

			if(c==',') p++;			
		}
	}

	// set tag data
	zinc->nColumns=nColumns;
	zinc->sTags=sTags;

	return 0;
}

// zinc_decode_rows
// Call to decode zinc grid rows

static int zinc_decode_rows(ZINC *zinc)
{
	// mark start of data
	char *p=zinc->pParse;

	// count rows
	int nRows=0;

	if(*p!=0)
	{
		do nRows++;
		while(zinc_next_line(zinc)==0);
	}

	// must have rows
	if(nRows==0)
	{
		engine_error("no zinc rows found");
		return -1;
	}

	// allocate grid
	char **sGrid=(char **) malloc(sizeof(char *)*zinc->nColumns*nRows);

	if(sGrid==NULL)
	{
		engine_error("out of memory allocating zinc grid");
		return -1;
	}

	// set grid data
	char c;

	int nRow;
	int nColumn;

	for(nRow=0;nRow<nRows;nRow++)
	{
		// out of data?
		if(*p==0)
		{
			free(sGrid);

			engine_error("not enough data for zinc grid");
			return -1;
		}

		// get column data for row
		int eol=0;

		for(nColumn=0;nColumn<zinc->nColumns;nColumn++)
		{
			// set data pointer
			sGrid[(nRow*zinc->nColumns)+nColumn]=p;

			if(eol==0)
			{
				// find next column
				while((c=*p)!=0 && c!=',' && c!='\n' && c!='\r')
				{
					p++;

					// skip string literals
					if(c=='\"' || c=='\'')
					{
						while(*p!=0 && *p!=c)
						{
							if(*p=='\\') p++;
							if(*p!=0) p++;
						}
						if(*p!=0) p++;
					}
					else
					{
						// skip arrays
						if(c=='[')
						{
							while(*p!=0 && *p!=']') p++;
							if(*p!=0) p++;
						}
						else
						{
							// skip zinc objects
							if(c=='{')
							{
								while(*p!=0 && *p!='}') p++;
								if(*p!=0) p++;
							}
						}
					}
				}
				*p=0;

				if(c=='\n' || c=='\r') eol=1;
				else if(c!=0) p++;
			}
		}

		if(eol==1) p++;
		else while((c=*p)!=0 && c!='\n' && c!='\r') p++;

		// skip to next line
		while((c=*p)!=0 && (c=='\n' || c=='\r')) p++;
	}

	// set grid data
	zinc->nRows=nRows;
	zinc->sGrid=sGrid;

	return 0;
}

// zinc_next_line
// Call to get next line

static int zinc_next_line(ZINC *zinc)
{
	char c;

	while((c=*zinc->pParse)!=0 && c!='\r' && c!='\n')
		zinc->pParse++;

	return zinc_skip_blank(zinc);
}

// zinc_skip_blank
// Call to skip blank lines

static int zinc_skip_blank(ZINC *zinc)
{
	char c;

	while((c=*zinc->pParse)!=0 && (c=='\r' || c=='\n'))
		zinc->pParse++;

	return (c==0)?-1:0;
}
