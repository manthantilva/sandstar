// io.h

#ifndef __IO_H__
#define __IO_H__

// Definitions

#define IO_MAXPATH		128
#define IO_MAXBUFFER	32

// Public functions

int io_exists(char *sDevice);

int io_open(char *sDevice,int nOpen);
int io_close(int hDevice);

int io_read(char *sDevice,char *sBuffer);
int io_write(char *sDevice,char *sBuffer);

int io_decode(char *sDevice,char *sResponses[],int *value);

const char *io_ext(const char *sDevice);

#endif // __IO_H__
