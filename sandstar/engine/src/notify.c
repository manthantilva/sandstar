// notify.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "engine.h"
#include "table.h"
#include "value.h"
#include "notify.h"

// Local functions

static int notify_find(NOTIFY *notify,key_t sender);
static int notify_next(NOTIFY *notify);

// Public functions

// notify_init
// Call to initialize notify controller

int notify_init(NOTIFY *notify,int nItems)
{
	int nSize=nItems*sizeof(NOTIFY_ITEM);

	notify->nItems=nItems;
	notify->nCount=0;

	notify->items=(NOTIFY_ITEM *) malloc(nSize);
	memset(notify->items,0,nSize);

	return 0;
}

// notify_exit
// Call to release notify controller

int notify_exit(NOTIFY *notify)
{
	// free items?
	if(notify->items!=NULL)
	{
		free(notify->items);
		notify->items=NULL;
	}

	notify->nItems=0;
	notify->nCount=0;

	return 0;
}

// notify_add
// Call to add item to notify list

int notify_add(NOTIFY *notify,key_t sender)
{
	// already notifying?
	int n=notify_find(notify,sender);
	if(n>=0) return 0;

	// get next free
	n=notify_next(notify);

	if(n<0)
	{
		engine_error("out of notify space");
		return -1;
	}

	// enable notify item
	NOTIFY_ITEM *item=&notify->items[n];

	item->nUsed=1;
	item->sender=sender;

	// add to count
	notify->nCount++;

	return 0;
}

// notify_remove
// Call to remove notify item

int notify_remove(NOTIFY *notify,key_t sender)
{
	// already notifying?
	int n=notify_find(notify,sender);

	if(n>=0)
	{
		// mark as unused
		notify->items[n].nUsed=0;
		notify->nCount--;

		return 0;
	}
	return -1;
}

// notify_send
// Call to send notify message

int notify_send(key_t sender,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// queue found?
	int hQueue=msgget(sender,0666);

	if(hQueue>=0)
	{
		// prep message
		ENGINE_MESSAGE msg;
		memset(&msg,0,sizeof(ENGINE_MESSAGE));

		msg.nMessage=ENGINE_MESSAGE_CHANGE;
		msg.channel=channel;
		msg.value=*value;

		// send message?
		if(msgsnd(hQueue,&msg,ENGINE_MESSAGE_SIZE,0)<0)
		{
//			engine_error("failed to queue message");
//			return -1;
		}
		return 0;
	}
	// else error

	return -1;
}

// notify_update
// Call to update notifications

int notify_update(NOTIFY *notify,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// find notifies
	int n;

	for(n=0;n<notify->nItems;n++)
	{
		NOTIFY_ITEM *item=&notify->items[n];

		// notify found?
		if(item->nUsed!=0)
			notify_send(item->sender,channel,value);
	}
	return 0;
}

// notify_report
// Call to display list of active notifies

int notify_report(NOTIFY *notify)
{
	// output header
	printf("\nsender\n");
	printf("----------\n");

	// output notifies
	int n;

	for(n=0;n<notify->nItems;n++)
	{
		NOTIFY_ITEM *item=&notify->items[n];

		// item in use?
		if(item->nUsed!=0)
		{
			printf("0x%-8.8X\n",
				item->sender);
		}
	}
	printf("\n");

	return 0;
}

// Local functions

// notify_find
// Call to find existing notify item

static int notify_find(NOTIFY *notify,key_t sender)
{
	int n;

	for(n=0;n<notify->nItems;n++)
	{
		// item found?
		NOTIFY_ITEM *item=&notify->items[n];

		if(item->nUsed!=0 && item->sender==sender)
			return n;
	}
	return -1;
}

// notify_next
// Call to find first free notify item

static int notify_next(NOTIFY *notify)
{
	int n;

	for(n=0;n<notify->nItems;n++)
	{
		// item not in use?
		if(notify->items[n].nUsed==0)
			return n;
	}
	return -1;
}
