// engine.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "global.h"
#include "engine.h"
#include "zinc.h"
#include "csv.h"
#include "table.h"
#include "value.h"
#include "anio.h"
#include "gpio.h"
#include "pwmio.h"
#include "i2cio.h"
#include "channel.h"
#include "watch.h"
#include "notify.h"
#include "poll.h"

// Definitions

#define MAX_ARG			128

// Default args

#define DEFAULT_ZINC	"/home/eacio/sandstar/EacIo/database.zinc"
#define DEFAULT_POINTS	"/home/eacio/sandstar/EacIo/points.csv"
#define DEFAULT_TABLES	"/home/eacio/sandstar/EacIo/tables.csv"

#define DEFAULT_PERIOD	1000

// Argmument holder

struct _ARGS
{
	int nCmd;

	char sZinc[MAX_ARG+1];
	char sCsv[MAX_ARG+1];
	char sPoints[MAX_ARG+1];
	char sTables[MAX_ARG+1];

	char sAddresses[MAX_ARG+1];
	char sTags[MAX_ARG+1];

	int nPeriod;
	int nDevice;
	int nTags;
};

typedef struct _ARGS ARGS;

// Command definition

struct _CMDS
{
	char *sCmd;
	int (*fnCmd)(ARGS *);
};

typedef struct _CMDS CMDS;

// Local functions

static int args_init(ARGS *args);

static int engine_help(ARGS *args);
static int engine_version(ARGS *args);
static int engine_start(ARGS *args);
static int engine_stop(ARGS *args);
static int engine_restart(ARGS *args);
static int engine_status(ARGS *args);
static int engine_channels(ARGS *args);
static int engine_tables(ARGS *args);
static int engine_polls(ARGS *args);
static int engine_watches(ARGS *args);
static int engine_notifies(ARGS *args);
static int engine_analog(ARGS *args);
static int engine_digital(ARGS *args);
static int engine_pwm(ARGS *args);
static int engine_i2c(ARGS *args);
static int engine_zinc(ARGS *args);
static int engine_csv(ARGS *args);
//static int engine_virtuals(ARGS *args);

//static int engine_load(ARGS *args,CHANNEL *channels,VIRTUAL_CHANNEL *virtualChannels,TABLE *tables,POLL *poll);
static int engine_load(ARGS *args,CHANNEL *channels,TABLE *tables,POLL *poll);
static int engine_loop(ARGS *args);

static void *engine_poll(void *arg);

static int engine_message(int nMessage);
static int engine_message_wait(int nMessage);

static int engine_reply(int nMessage,ENGINE_MESSAGE *msg);

// Local data

static CMDS g_cmds[]=
{
	{"help",engine_help},
	{"version",engine_version},
	{"start",engine_start},
	{"stop",engine_stop},
	{"restart",engine_restart},
	{"status",engine_status},
	{"channels",engine_channels},
	{"tables",engine_tables},
	{"polls",engine_polls},
	{"watches",engine_watches},
	{"notifies",engine_notifies},
	{"analog",engine_analog},
	{"digital",engine_digital},
	{"pwm",engine_pwm},
	{"i2c",engine_i2c},
	{"zinc",engine_zinc},
	{"csv",engine_csv},
	//{"virtuals",engine_virtuals},
	{NULL}
};

static int g_quiet=0;
static int g_quit=0;

static int g_hEngine=-1;

// Public functions

// main
// Called at program startup

void logToFile(const char* msg)
{
#if 0
    char *name = "/tmp/c.log";
    FILE* fd = fopen(name, "a");
    if (fd == NULL) {
        perror("open failed");
        return;
    }
	fprintf(fd,"%s\n",msg);
	fclose(fd);
#endif
}

int main(int argc,char *argv[])
{
    // int fd;
    // char *name = "/tmp/p.log";
    // fd = fopen(name, O_WRONLY | O_CREAT , 0644);
    // if (fd == -1) {
        // perror("open failed");
        // exit(1);
    // }

    // if (dup2(fd, 1) == -1) {
        // perror("dup2 failed"); 
        // exit(1);
    // }

	// setup args
	static ARGS args;
	args_init(&args);

	// process cmd line
	int n;

	for(n=1;n<argc;n++)
	{
		char *sArg=argv[n];

		int nFound=0;
		int c;

		// find command match?
		for(c=0;g_cmds[c].sCmd!=NULL;c++)
		{
			if(strcmp(sArg,g_cmds[c].sCmd)==0)
			{
				args.nCmd=c;
				nFound++;

				break;
			}
		}

		// find flag match?
		if(nFound==0 && sArg[0]=='-')
		{
			nFound=1;

			// what flag?
			switch(sArg[1])
			{
				case 'h':
				case 'H':
				case '?':
					// show help
					args.nCmd=0;
					break;

				case 'q':
					// quiet mode
					g_quiet=1;
					break;

				case 'v':
					// verbose mode
					g_quiet=-1;
					break;

				case 'F':
					// poll frequency (ms)
					args.nPeriod=strtol(sArg+2,NULL,10);
					break;

				case 'D':
					// device number
					args.nDevice=strtol(sArg+2,NULL,10);
					break;

				case 'A':
					// address range
					strncpy(args.sAddresses,sArg+2,MAX_ARG);
					break;

				case 'Z':
					// zinc file
					strncpy(args.sZinc,sArg+2,MAX_ARG);
					break;

				case 'C':
					// csv file
					strncpy(args.sCsv,sArg+2,MAX_ARG);
					break;

				case 'P':
					// point file
					strncpy(args.sPoints,sArg+2,MAX_ARG);
					break;

				case 'L':
					// tables file
					strncpy(args.sTables,sArg+2,MAX_ARG);
					break;

				case 't':
					// list all tags
					args.nTags=1;
					break;

				case 'T':
					// list specific tag data
					snprintf(args.sTags,MAX_ARG,",%s,",sArg+2);
					break;

				default:
					// not found
					nFound=0;
					break;
			}
		}

		// arg not found?
		if(nFound==0)
		{
			engine_error("illegal option -- %s",sArg);
			args.nCmd=0;

			break;
		}
	}

	// execute command
	logToFile(g_cmds[args.nCmd].sCmd);
	int ret = (g_cmds[args.nCmd].fnCmd)(&args);
	// close(fd);
	return ret;
}

// engine_error
// Call to display engine error

int engine_error(char *sFormat,...)
{
    va_list args;

	// not in quiet mode?
	if(g_quiet<=0)
	{
		// output error
		printf("engine: error: ");

		va_start(args,sFormat);
		vprintf(sFormat,args);
		va_end(args);

		printf("\n");
	}
	return -1;
}

// Local functions

static int args_init(ARGS *args)
{
	// defaults
	memset(args,0,sizeof(ARGS));

	strncpy(args->sZinc,DEFAULT_ZINC,MAX_ARG);
	strncpy(args->sCsv,DEFAULT_POINTS,MAX_ARG);
	strncpy(args->sPoints,DEFAULT_POINTS,MAX_ARG);
	strncpy(args->sTables,DEFAULT_TABLES,MAX_ARG);

	args->nPeriod=DEFAULT_PERIOD;

	return 0;
}

// engine_help
// Called to process help command

static int engine_help(ARGS *args)
{
	printf("\nUsage:\n");
	printf(" engine [command] [-qvFDAZCPLtT]\n\n");

	printf("Commands:\n");
	printf(" help             display this page\n");
	printf(" version          display engine version\n\n");

	printf(" start [-qvFZPL]  start engine task\n");
	printf(" stop             stop engine task\n");
	printf(" restart          restart engine task\n\n");

	printf(" status           display current status\n");
	printf(" channels         display current channel list\n");
	printf(" tables           display current tables list\n");
	printf(" polls            display current poll list\n");
	printf(" watches          display current watch list\n");
	printf(" notifies         display current notify list\n\n");

	printf(" analog [-DA]     display analog input values\n");
	printf(" digital [-DA]    display digital input values\n");
	printf(" pwm [-DA]        display pwm output values\n");
	printf(" i2c [-DA]        display i2c input values\n\n");

	printf(" zinc [-ZtT]      display zinc file info\n");
	printf(" csv [-CtT]       display csv file info\n\n");

	printf("Options:\n");
	printf(" -q               quiet mode\n");
	printf(" -v               verbose mode\n");
	printf(" -Fn              specify poll frequency in ms\n");
	printf(" -Dn              specify device number\n");
	printf(" -An[-n][,n]      specify address range\n");
	printf(" -Zfile           specify zinc file location\n");
	printf(" -Cfile           specify csv file location\n");
	printf(" -Pfile           specify points file location\n");
	printf(" -Lfile           specify tables file location\n");
	printf(" -t               list file tags\n");
	printf(" -Ttag[,tag]      list file tag values\n\n");

	return 0;
}

// engine_version
// Called to process version command

static int engine_version(ARGS *args)
{
	printf("\n version: %d.%d\n",ENGINE_VERSION_MAJ,ENGINE_VERSION_MIN);
	printf("    date: %s %s\n",__DATE__,__TIME__);
	printf("   short: %d bit\n",(int) (sizeof(short)*8));
	printf("     int: %d bit\n",(int) (sizeof(int)*8));
	printf("    long: %d bit\n\n",(int) (sizeof(long)*8));

	return 0;
}

// engine_start
// Called to process start command

static int engine_start(ARGS *args)
{
	// setup hardware
	pwmio_config_pwm();
	
	// queue found?
	int hQueue=msgget(ENGINE_MESSAGE_KEY,0666);

	if(hQueue>=0)
	{
		engine_error("engine already running");
		// return -1;
    }
	else {
		// create queue?
		g_hEngine=msgget(ENGINE_MESSAGE_KEY,IPC_CREAT|0666);

		if(g_hEngine<0)
		{
			engine_error("failed to create message queue");
			return -1;
		}
	}


	// fork child process
	logToFile("Creating daemon\n");
	pid_t pid=fork();

	if(pid<0)
	{
		engine_error("failed to create process fork");
		return -1;
	}

	// stop parent process
	if(pid>0) return 0;

    // int fd;
	logToFile("Log file set\n");

	// start polling thread?
	pthread_t pollthread;
	
	logToFile("Creating thread\n");

	if(pthread_create(&pollthread,NULL,engine_poll,(void *) args)<0) 
	{
		logToFile("failed to create poll thread");
		return -1;
	}

	
	logToFile("running loop\n");
	
	// run engine loop
	int err=engine_loop(args);

	// wait for poll thread
	if(pthread_join(pollthread,NULL)<0)
	{
		printf("failed to join poll thread");
		return -1;
	}
logToFile("thread joined\n");
	// kill queue
	if(msgctl(g_hEngine,IPC_RMID,0)<0)
	{
		printf("failed to destroy message queue");
		return -1;
	}
logToFile("queue deleted\n");

// fclose(fd);
	return err;
}

// engine_stop
// Called to process stop command

static int engine_stop(ARGS *args)
{
	// printf("engine_stop in engine\n");
	// signal process to stop
	logToFile("engine_stop 1");
	int ret = engine_message(ENGINE_MESSAGE_STOP);
	if(ret == 0){
		logToFile("stop send success");
	}
	else {
		logToFile("stop send failed");
	}
	
	return ret;
}

// engine_restart
// Called to process restart command

static int engine_restart(ARGS *args)
{
	// signal for restart
	return engine_message(ENGINE_MESSAGE_RESTART);
}

// engine_status
// Called to process status command

static int engine_status(ARGS *args)
{
	// signal for status
	return engine_message_wait(ENGINE_MESSAGE_STATUS);
}

// engine_channels
// Called to process channels command

static int engine_channels(ARGS *args)
{
	// signal for channels
	return engine_message_wait(ENGINE_MESSAGE_CHANNELS);
}

// engine_tables
// Called to process tables command

static int engine_tables(ARGS *args)
{
	// signal for tables
	return engine_message_wait(ENGINE_MESSAGE_TABLES);
}

// engine_polls
// Called to process polls command

static int engine_polls(ARGS *args)
{
	// signal for polls
	return engine_message_wait(ENGINE_MESSAGE_POLLS);
}

// engine_watches
// Called to process watches command

static int engine_watches(ARGS *args)
{
	// signal for watches
	return engine_message_wait(ENGINE_MESSAGE_WATCHES);
}

// engine_notifies
// Called to process notifies command

static int engine_notifies(ARGS *args)
{
	// signal for notifies
	return engine_message_wait(ENGINE_MESSAGE_NOTIFIES);
}

// engine_analog
// Called to process analog command

static int engine_analog(ARGS *args)
{
	char sBuffer[16];

	g_quiet++;

	// output header
	printf("\naddress status  value\n");
	printf("------- ------- -------\n");

	// process analogs
	char *sAddresses=args->sAddresses;
	char *sAddress;

	while((sAddress=strsep(&sAddresses,","))!=NULL)
	{
		if(sAddress[0]==0) continue;

		// get analog range
		char *sRange=sAddress;

		char *sStart=strsep(&sRange,"-");
		char *sStop=strsep(&sRange,"-");

		ANIO_ADDRESS start=strtol(sStart,NULL,10);
		ANIO_ADDRESS stop=(sStop==NULL)?start:strtol(sStop,NULL,10);

		ANIO_DEVICE device=(ANIO_DEVICE) args->nDevice;
		ANIO_ADDRESS address;
		ANIO_VALUE value;

		// output analogs
		for(address=start;address<=stop;address++)
		{
			char *sStatus="error";
			char *sValue="n/a";

			// get analog value?
			if(anio_get_value(device,address,&value)>=0)
			{
				snprintf(sBuffer,sizeof(sBuffer),"%d",(int) value);

				sValue=sBuffer;
				sStatus="ok";
			}

			// output results
			printf("%-7d %-7.7s %s\n",address,sStatus,sValue);
		}
	}
	printf("\n");

	g_quiet--;

	return 0;
}

// engine_digital
// Called to process digital command

static int engine_digital(ARGS *args)
{
	static char *g_sDirection[]={"in","out","high","low"};
	static char *g_sEdge[]={"none","rising","falling","both"};
	static char *g_sActivelow[]={"no","yes"};

	char sBuffer[16];

	g_quiet++;

	// output header
	printf("\naddress status  dir     edge    actlow  value\n");
	printf("------- ------- ------- ------- ------- -------\n");

	// process digitals
	char *sAddresses=args->sAddresses;
	char *sAddress;

	while((sAddress=strsep(&sAddresses,","))!=NULL)
	{
		if(sAddress[0]==0) continue;

		// get digital range
		char *sRange=sAddress;

		char *sStart=strsep(&sRange,"-");
		char *sStop=strsep(&sRange,"-");

		GPIO_ADDRESS start=strtol(sStart,NULL,10);
		GPIO_ADDRESS stop=(sStop==NULL)?start:strtol(sStop,NULL,10);

		GPIO_ADDRESS address;
		GPIO_DIRECTION direction;
		GPIO_EDGE edge;
		GPIO_ACTIVELOW activelow;
		GPIO_VALUE value;

		// output digitals
		for(address=start;address<=stop;address++)
		{
			char *sStatus="error";
			char *sDirection="n/a";
			char *sEdge="n/a";
			char *sActivelow="n/a";
			char *sValue="n/a";

			int err=0;
			int exp=0;

			if(gpio_exists(address)<0)
			{
				// export gpio address?
				if(gpio_export(address)>=0) exp=1;
				else err=1;
			}

			// get gpio info?
			if(err==0)
			{
				// get direction?
				if(gpio_get_direction(address,&direction)>=0)
					sDirection=g_sDirection[(int) direction];

				// get edge?
				if(gpio_get_edge(address,&edge)>=0)
					sEdge=g_sEdge[(int) edge];

				// get active low?
				if(gpio_get_activelow(address,&activelow)>=0)
					sActivelow=g_sActivelow[(int) activelow];

				// get value?
				if(gpio_get_value(address,&value)>=0)
				{
					snprintf(sBuffer,sizeof(sBuffer),"%d",(int) value);

					sValue=sBuffer;
					sStatus="ok";
				}
			}

			// unexport gpio address?
			if(exp==1) gpio_unexport(address);

			// output results
			printf("%-7d %-7.7s %-7.7s %-7.7s %-7.7s %s\n",address,sStatus,sDirection,sEdge,sActivelow,sValue);
		}
	}
	printf("\n");

	g_quiet--;

	return 0;
}

// engine_pwm
// Called to process pwm command

static int engine_pwm(ARGS *args)
{
	static char *g_sPolarity[]={"normal","invert"};
	static char *g_sEnable[]={"no","yes"};

	char sBuffer1[16];
	char sBuffer2[16];

	g_quiet++;

	// output header
	printf("\naddress status  polarity period  duty    enabled\n");
	printf("------- ------- -------- ------- ------- -------\n");

	// process pwms
	char *sAddresses=args->sAddresses;
	char *sAddress;

	while((sAddress=strsep(&sAddresses,","))!=NULL)
	{
		if(sAddress[0]==0) continue;

		// get digital range
		char *sRange=sAddress;

		char *sStart=strsep(&sRange,"-");
		char *sStop=strsep(&sRange,"-");

		PWMIO_CHANNEL start=strtol(sStart,NULL,10);
		PWMIO_CHANNEL stop=(sStop==NULL)?start:strtol(sStop,NULL,10);

		PWMIO_CHIP chip=(PWMIO_CHIP) args->nDevice;
		PWMIO_CHANNEL channel;
		PWMIO_POLARITY polarity;
		PWMIO_PERIOD period;
		PWMIO_DUTY duty;
		PWMIO_ENABLE enable;

		// output digitals
		for(channel=start;channel<=stop;channel++)
		{
			char *sStatus="error";
			char *sPolarity="n/a";
			char *sPeriod="n/a";
			char *sDuty="n/a";
			char *sEnable="n/a";

			int err=0;
			int exp=0;

			if(pwmio_exists(chip,channel)<0)
			{
				// export pwm channel?
				if(pwmio_export(chip,channel)>=0) exp=1;
				else err=1;
			}

			// get pwm info?
			if(err==0)
			{
				// get polarity?
				if(pwmio_get_polarity(chip,channel,&polarity)>=0)
					sPolarity=g_sPolarity[(int) polarity];

				// get period?
				if(pwmio_get_period(chip,channel,&period)>=0)
				{
					snprintf(sBuffer1,sizeof(sBuffer1),"%d",period);
					sPeriod=sBuffer1;
				}

				// get duty?
				if(pwmio_get_duty(chip,channel,&duty)>=0)
				{
					snprintf(sBuffer2,sizeof(sBuffer2),"%d",duty);
					sDuty=sBuffer2;
				}

				// get enable?
				if(pwmio_get_enable(chip,channel,&enable)>=0)
				{
					sEnable=g_sEnable[(int) enable];
					sStatus="ok";
				}
			}

			// unexport pwm channel?
			if(exp==1) pwmio_unexport(chip,channel);

			// output results
			printf("%-7d %-7.7s %-8.8s %-7.7s %-7.7s %s\n",channel,sStatus,sPolarity,sPeriod,sDuty,sEnable);
		}
	}
	printf("\n");

	g_quiet--;

	return 0;
}

// engine_i2c
// Called to process i2c command

static int engine_i2c(ARGS *args)
{
	char sBuffer[16];

	g_quiet++;

	// output header
	printf("\naddress status  value\n");
	printf("------- ------- -------\n");

	// process pwms
	char *sAddresses=args->sAddresses;
	char *sAddress;

	while((sAddress=strsep(&sAddresses,","))!=NULL)
	{
		if(sAddress[0]==0) continue;

		// get analog range
		char *sRange=sAddress;

		char *sStart=strsep(&sRange,"-");
		char *sStop=strsep(&sRange,"-");

		I2CIO_ADDRESS start=strtol(sStart,NULL,10);
		I2CIO_ADDRESS stop=(sStop==NULL)?start:strtol(sStop,NULL,10);

		I2CIO_DEVICE device=(I2CIO_DEVICE) args->nDevice;
		I2CIO_ADDRESS address;
		I2CIO_VALUE value;

		// output analogs
		for(address=start;address<=stop;address++)
		{
			char *sStatus="error";
			char *sValue="n/a";

			// get i2c measurement?
			if(i2cio_get_measurement(device,address,&value)>=0)
			{
				snprintf(sBuffer,sizeof(sBuffer),"%d",(int) value);

				sValue=sBuffer;
				sStatus="ok";
			}

			// output results
			printf("%-7d %-7.7s %s\n",address,sStatus,sValue);
		}
	}
	printf("\n");

	g_quiet--;

	return 0;
}

// engine_zinc
// Called to process zinc command

static int engine_zinc(ARGS *args)
{
	// init zinc desc
	ZINC zinc;
	zinc_init(&zinc);

	// load zinc file?
	int err=zinc_load(&zinc,args->sZinc);

	if(err>=0)
	{
		// display what?
		int nDetails=0;

		if(args->nTags==1) nDetails=1;
		if(args->sTags[0]!=0) nDetails=2;

		switch(nDetails)
		{
			case 0:
				// file report
				err=zinc_report(&zinc,args->sZinc);
				break;

			case 1:
				// tag list
				err=zinc_report_tags(&zinc);
				break;

			case 2:
				// grid display
				err=zinc_report_grid(&zinc,args->sTags);
				break;
		}
	}

	// release it
	zinc_exit(&zinc);

	return err;
}

// engine_csv
// Called to process csv command

static int engine_csv(ARGS *args)
{
	// init csv desc
	CSV csv;
	csv_init(&csv);

	// load csv file?
	int err=csv_load(&csv,args->sCsv);

	if(err>=0)
	{
		// display what?
		int nDetails=0;

		if(args->nTags==1) nDetails=1;
		if(args->sTags[0]!=0) nDetails=2;

		switch(nDetails)
		{
			case 0:
				// file report
				err=csv_report(&csv,args->sCsv);
				break;

			case 1:
				// column list
				err=csv_report_columns(&csv);
				break;

			case 2:
				// value display
				err=csv_report_values(&csv,args->sTags);
				break;
		}
	}

	// release it
	csv_exit(&csv);

	return err;
}

// engine_virtuals
// Called to process virtual channels command

//static int engine_virtuals(ARGS *args)
//{
//	// signal for channels
//	return engine_message_wait(ENGINE_MESSAGE_VIRTUALS);
//}

// engine_load
// Call to load poll addresses

//static int engine_load(ARGS *args,CHANNEL *channels,VIRTUAL_CHANNEL *virtualChannels,TABLE *tables,POLL *poll)
static int engine_load(ARGS *args,CHANNEL *channels,TABLE *tables,POLL *poll)
{
	// initialize data
	channel_init(channels,MAX_CHANNELS);
	//virtual_channel_init(virtualChannels,MAX_VIRTUAL_CHANNELS);
	table_init(tables,MAX_TABLES);
	poll_init(poll,MAX_POLLS);

	// load tables?
	int err=table_load(tables,args->sTables);
	if(err<0) return err;

	// init zinc desc
	ZINC zinc;
	zinc_init(&zinc);

	// load zinc file?
	err=zinc_load(&zinc,args->sZinc);

	if(err>=0)
	{
		// init point desc
		CSV points;
		csv_init(&points);

		// load points file?
		err=csv_load(&points,args->sPoints);

		if(err>=0)
		{
			// process zinc file
			int nRow1; // refers to zinc file rows
			int nRow2; // refers to points.csv rows 

			for(nRow1=0;nRow1<zinc.nRows;nRow1++)
			{
				// get channel number
				ENGINE_CHANNEL channel=zinc_integer(&zinc,nRow1,"channel",-1);

				if(zinc_marker(&zinc,nRow1,"virtualChannel",0)==1)
				{
					// get point data
					CHANNEL_ENABLE enable=CHANNEL_ENABLE_DISABLED;

					if(zinc_marker(&zinc,nRow1,"enabled",0)==1 &&
						zinc_marker(&zinc,nRow1,"disabled",0)==0)
						enable=CHANNEL_ENABLE_ENABLED;

					//CHANNEL_TYPE type=csv_list(&points,nRow2,"type",g_sChannelType);
					CHANNEL_TYPE type;
					if (zinc_marker(&zinc,nRow1,"analog",0)==1)
					{
						type=CHANNEL_TYPE_VIRTUAL_ANALOG;
					}
					else if (zinc_marker(&zinc,nRow1,"binary",0)==1) // default binary
					{
						type=CHANNEL_TYPE_VIRTUAL_DIGITAL;
					}
					else
					{
						//TODO: Remove this case
						type=CHANNEL_TYPE_VIRTUAL_DIGITAL;
					}
					
					//CHANNEL_DIRECTION direction=csv_list(&points,nRow2,"direction",g_sChannelDirection);
					// TODO: Should be OUT. The OUT direction is assigned to fix
					// channel_write() function. Remove OUT and use NONE instead.
					CHANNEL_DIRECTION direction=CHANNEL_DIRECTION_OUT;
					
					//char *sDevice=csv_string(&points,nRow2,"device");
					//char *sDevice=NULL;
					
					//CHANNEL_ADDRESS address=csv_integer(&points,nRow2,"address");
					CHANNEL_ADDRESS address=-1;

					//int nTrigger=csv_integer(&points,nRow2,"trigger");
					int nTrigger=0;

					// resolve device address?
					CHANNEL_DEVICE device=-1;

					// setup convertion
					VALUE_CONV conv;

					conv.nTable=-1;
					conv.flags=0;

					// using low?
					if(zinc_null(&zinc,nRow1,"low",1)==0)
					{
						conv.low=zinc_number(&zinc,nRow1,"low",0.0);
						conv.flags|=VALUE_CONV_USELOW;
					}

					// using high?
					if(zinc_null(&zinc,nRow1,"high",1)==0)
					{
						conv.high=zinc_number(&zinc,nRow1,"high",1.0);
						conv.flags|=VALUE_CONV_USEHIGH;
					}

					// using offset?
					if(zinc_null(&zinc,nRow1,"offset",1)==0)
					{
						conv.offset=zinc_number(&zinc,nRow1,"offset",0.0);
						conv.flags|=VALUE_CONV_USEOFFSET;
					}

					// using scale?
					if(zinc_null(&zinc,nRow1,"scale",1)==0)
					{
						conv.scale=zinc_number(&zinc,nRow1,"scale",1.0);
						conv.flags|=VALUE_CONV_USESCALE;
					}

					// using min?
					if(zinc_marker(&zinc,nRow1,"min",0)==1)
					{
						conv.min=zinc_number(&zinc,nRow1,"minVal",0.0);
						conv.flags|=VALUE_CONV_USEMIN;
					}

					// using max?
					if(zinc_marker(&zinc,nRow1,"max",0)==1)
					{
						conv.max=zinc_number(&zinc,nRow1,"maxVal",1.0);
						conv.flags|=VALUE_CONV_USEMAX;
					}

					// locate conv table
					//char *sUnit=zinc_string(&zinc,nRow1,"unit","");

					// analog to digital conv?
					if(type==CHANNEL_TYPE_ANALOG &&
						zinc_marker(&zinc,nRow1,"binary",0)==1)
						conv.flags|=VALUE_CONV_ADC;

					// add to channel list
					channel_add(channels,channel,enable,type,direction,device,address,nTrigger,&conv);

					// add poll if wanted
					//if((zinc_marker(&zinc,nRow1,"cur",0)==1 ||
					//	zinc_marker(&zinc,nRow1,"raw",0)==1))
						poll_add(poll,channel);
					
					// add to virtual channel list
					//virtual_channel_add(virtualChannels,channel,enable,type);
				}
				else
				{
					// find channel in points
					for(nRow2=0;nRow2<points.nRows;nRow2++)
					{
						// found channel?
						ENGINE_CHANNEL c=csv_integer(&points,nRow2,"channel");

						if(c==channel)
						{
							// get point data
							CHANNEL_ENABLE enable=CHANNEL_ENABLE_DISABLED;

							if(zinc_marker(&zinc,nRow1,"enabled",0)==1 &&
								zinc_marker(&zinc,nRow1,"disabled",0)==0)
								enable=CHANNEL_ENABLE_ENABLED;

							CHANNEL_TYPE type=csv_list(&points,nRow2,"type",g_sChannelType);
							CHANNEL_DIRECTION direction=csv_list(&points,nRow2,"direction",g_sChannelDirection);
							char *sDevice=csv_string(&points,nRow2,"device");
							CHANNEL_ADDRESS address=csv_integer(&points,nRow2,"address");

							int nTrigger=csv_integer(&points,nRow2,"trigger");

							// resolve device address?
							CHANNEL_DEVICE device;

							if(*sDevice=='$')
							{
								int res=-1;

								switch(type)
								{
									case CHANNEL_TYPE_PWM:
										// resolve pwm from address
										res=pwmio_resolve(sDevice+1,(PWMIO_CHIP *) &device);
										break;

									default:
										// ignore others
										break;
								}

								if(res<0)
								{
									engine_error("failed to resolve channel %d device address",channel);
									break;
								}
							}
							else device=strtol(sDevice,NULL,10);

							// setup convertion
							VALUE_CONV conv;

							conv.nTable=-1;
							conv.flags=0;

							// using low?
							if(zinc_null(&zinc,nRow1,"low",1)==0)
							{
								conv.low=zinc_number(&zinc,nRow1,"low",0.0);
								conv.flags|=VALUE_CONV_USELOW;
							}

							// using high?
							if(zinc_null(&zinc,nRow1,"high",1)==0)
							{
								conv.high=zinc_number(&zinc,nRow1,"high",1.0);
								conv.flags|=VALUE_CONV_USEHIGH;
							}

							// using offset?
							if(zinc_null(&zinc,nRow1,"offset",1)==0)
							{
								conv.offset=zinc_number(&zinc,nRow1,"offset",0.0);
								conv.flags|=VALUE_CONV_USEOFFSET;
							}

							// using scale?
							if(zinc_null(&zinc,nRow1,"scale",1)==0)
							{
								conv.scale=zinc_number(&zinc,nRow1,"scale",1.0);
								conv.flags|=VALUE_CONV_USESCALE;
							}

							// using min?
							if(zinc_marker(&zinc,nRow1,"min",0)==1)
							{
								conv.min=zinc_number(&zinc,nRow1,"minVal",0.0);
								conv.flags|=VALUE_CONV_USEMIN;
							}

							// using max?
							if(zinc_marker(&zinc,nRow1,"max",0)==1)
							{
								conv.max=zinc_number(&zinc,nRow1,"maxVal",1.0);
								conv.flags|=VALUE_CONV_USEMAX;
							}

							// locate conv table
							char *sUnit=zinc_string(&zinc,nRow1,"unit","");
							int n;

							for(n=0;n<tables->nItems;n++)
							{
								// item found?
								TABLE_ITEM *item=&tables->items[n];

								if(item->nUsed!=0 &&
									strcmp(item->sUnit,sUnit)==0 &&
									zinc_marker(&zinc,nRow1,item->sTag,0)==1)
								{
									conv.nTable=n;
									break;
								}
							}

							// analog to digital conv?
							if(type==CHANNEL_TYPE_ANALOG &&
								zinc_marker(&zinc,nRow1,"binary",0)==1)
								conv.flags|=VALUE_CONV_ADC;

							// add to channel list
							channel_add(channels,channel,enable,type,direction,device,address,nTrigger,&conv);

							// add poll if wanted
							if((zinc_marker(&zinc,nRow1,"cur",0)==1 ||
								zinc_marker(&zinc,nRow1,"raw",0)==1))
								poll_add(poll,channel);

							break;
						}
					}
				}
			}
		}

		// release csv desc
		csv_exit(&points);
	}

	// release zinc desc
	zinc_exit(&zinc);

	return err;
}

// engine_loop
// Call to process engine loop

static int engine_loop(ARGS *args)
{
	logToFile("engine_loop 1");
	// load data?
	CHANNEL channels;
	//VIRTUAL_CHANNEL virtualChannels;
	TABLE tables;
	POLL poll;

	//int err=engine_load(args,&channels,&virtualChannels,&tables,&poll);
	int err=engine_load(args,&channels,&tables,&poll);
logToFile("engine_loop 2");
	if(err<0)
	{
		logToFile("engine_loop 3");
		// release em
		poll_exit(&poll);
		table_exit(&tables);
		channel_exit(&channels);
		logToFile("engine_loop 4");
		//virtual_channel_exit(&virtualChannels);

		return -1;
	}
logToFile("engine_loop 5 ");
	// init watches
	WATCH watch;
	watch_init(&watch,MAX_WATCHES);
logToFile("engine_loop 6 ");
	// init notifies
	NOTIFY notify;
	notify_init(&notify,MAX_NOTIFIES);
logToFile("engine_loop 7 ");
	// enter main loop
	while(g_quit==0)
	{
		// received message?
		ENGINE_MESSAGE msg;
		logToFile("Inloop 1");
		err=msgrcv(g_hEngine,&msg,ENGINE_MESSAGE_SIZE,0,0);
		logToFile("Inloop 2");

		if(err>=0)
		{
			logToFile("RX MESSAGE");
			// what message?
			switch(msg.nMessage)
			{
				case ENGINE_MESSAGE_STOP:
					logToFile("RX ENGINE_MESSAGE_STOP");
					// stop message
					g_quit=1;
					break;

				case ENGINE_MESSAGE_RESTART:
					// reload data
					poll_exit(&poll);
					table_exit(&tables);
					channel_exit(&channels);
					//virtual_channel_exit(&virtualChannels);

					//engine_load(args,&channels,&virtualChannels,&tables,&poll);
					engine_load(args,&channels,&tables,&poll);
					break;

				case ENGINE_MESSAGE_POLL:
					// poll values quietly
					g_quiet++;
					poll_update(&poll,&channels,&tables,&watch,&notify);
					g_quiet--;
					break;

				case ENGINE_MESSAGE_STATUS:
					// status message
					printf("\n  status: running\n");
					//TODO: List real and virtual channels
					printf("channels: %d\n",channels.nCount);
					printf("  tables: %d\n",tables.nCount);
					printf("   polls: %d\n",poll.nCount);
					printf(" watches: %d\n",watch.nCount);
					printf("notifies: %d\n\n",notify.nCount);
					break;

				case ENGINE_MESSAGE_TABLES:
					// display tables
					table_report(&tables);
					break;

				case ENGINE_MESSAGE_CHANNELS:
					// display channels
					channel_report(&channels);
					break;

				case ENGINE_MESSAGE_POLLS:
					// display polls
					poll_report(&poll,&channels,&tables);
					break;

				case ENGINE_MESSAGE_WATCHES:
					// display watches
					watch_report(&watch);
					break;
				
				case ENGINE_MESSAGE_NOTIFIES:
					// display notifies
					notify_report(&notify);
					break;

				case ENGINE_MESSAGE_CONVERT:
					// convert data
					g_quiet++;
					channel_convert(&channels,msg.channel,&msg.value,&tables);
					g_quiet--;
					engine_reply(ENGINE_MESSAGE_CONVACK,&msg);
					break;

				case ENGINE_MESSAGE_NOTIFY:
					// notify message
					if(notify_add(&notify,msg.sender)>=0)
						poll_notify(&poll,msg.sender);
					break;

				case ENGINE_MESSAGE_UNNOTIFY:
					// unnotify message
					notify_remove(&notify,msg.sender);
					break;

				case ENGINE_MESSAGE_WATCH:
					// watch message
					if(watch_add(&watch,msg.channel,msg.sender)>=0)
						poll_watch(&poll,msg.channel,msg.sender);
					break;

				case ENGINE_MESSAGE_UNWATCH:
					// unwatch message
					watch_remove(&watch,msg.channel,msg.sender);
					break;

				case ENGINE_MESSAGE_READ:
					// read message
					g_quiet++;
					channel_read(&channels,msg.channel,&msg.value,&tables);
					g_quiet--;
					engine_reply(ENGINE_MESSAGE_READACK,&msg);
					break;

				case ENGINE_MESSAGE_WRITE:
					// write message
					g_quiet++;
					//printf("<<engine>> write channel:%d\n", msg.channel);
					channel_write(&channels,msg.channel,&msg.value,&tables);
					g_quiet--;
					engine_reply(ENGINE_MESSAGE_WRITEACK,&msg);
					break;

				//case ENGINE_MESSAGE_WRITE_VIRTUAL:
				//	virtual_channel_write(&virtualChannels,msg.channel,&msg.value);
				//	break;
					
				//case ENGINE_MESSAGE_VIRTUALS:
				//	// display virtual channels
				//	virtual_channel_report(&virtualChannels);
				//	break;
					
				default:
					// unknown
					engine_error("received unknown message (0x%-2.2lX)",msg.nMessage);
					break;
			}
		}
		// fflush(stdout);
		// fflush(stderr);
		// err!=ENOMSG ...
	}
	logToFile("Out of while 1");

	// release these
	watch_exit(&watch);
	notify_exit(&notify);

	poll_exit(&poll);
	table_exit(&tables);
	channel_exit(&channels);
	//virtual_channel_exit(&virtualChannels);

	return 0;
}

// engine_poll
// Call to process engine polls

static void *engine_poll(void *arg)
{
	ARGS *args=(ARGS *) arg;

	// micro seconds sleep
	useconds_t usecs=args->nPeriod*1000;

	// enter main loop
	while(g_quit==0)
	{
		// signal process to poll
		engine_message(ENGINE_MESSAGE_POLL);

		// sleep for a bit
		usleep(usecs);
		// fflush(stdout);
		// fflush(stderr);
	}
	return NULL;
}

// engine_message
// Call to signal engine process

static int engine_message(int nMessage)
{
	// queue found?
	int hQueue=msgget(ENGINE_MESSAGE_KEY,0666);

	if(hQueue<0)
	{
		engine_error("engine not running");
		return -1;
	}

	// prep message
	ENGINE_MESSAGE msg;
	memset(&msg,0,sizeof(ENGINE_MESSAGE));

	msg.nMessage=nMessage;

	// send message?
	if(msgsnd(hQueue,&msg,ENGINE_MESSAGE_SIZE,0)<0)
	{
		engine_error("failed to queue message");
		return -1;
	}
	// printf("MSG:%d send",nMessage);
	return 0;
}

// engine_message_wait
// Call to signal engine and wait

static int engine_message_wait(int nMessage)
{
	// send message?
	if(engine_message(nMessage)>=0)
	{
		// wait for output
		usleep(100000);
		return 0;
	}
	return -1;
}

// engine_reply
// Call to send message reply

static int engine_reply(int nMessage,ENGINE_MESSAGE *msg)
{
	// engine queue found?
	int hQueue=msgget(msg->sender,0666);

	if(hQueue<0)
	{
		engine_error("failed to get sender message queue");
		return -1;
	}

	// prep message
	msg->nMessage=nMessage;
	msg->sender=ENGINE_MESSAGE_KEY;

	// send message?
	if(msgsnd(hQueue,msg,ENGINE_MESSAGE_SIZE,0)<0)
	{
		engine_error("failed to queue message");
		return -1;
	}
	return 0;
}
