// gpio.c

// https://www.kernel.org/doc/Documentation/gpio/sysfs.txt

// Include files

#include <unistd.h>
#include <stdio.h>

#include "global.h"
#include "engine.h"
#include "io.h"
#include "gpio.h"

// Definitions

#define GPIO_SYSFS		"/sys/class/gpio"

// Local data

static char *g_sDirection[]={"in","out","high","low",NULL};
static char *g_sEdge[]={"none","rising","falling","both",NULL};
static char *g_sBoolean[]={"0","1",NULL};

// Public functions

// gpio_exists
// Call to check if gpio address exists

int gpio_exists(GPIO_ADDRESS address)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d",address);

	return io_exists(sDevice);
}

// gpio_export
// Call to export gpio address

int gpio_export(GPIO_ADDRESS address)
{
	char sAddress[IO_MAXBUFFER+1];
	snprintf(sAddress,IO_MAXBUFFER,"%d",address);

	return io_write(GPIO_SYSFS "/export",sAddress);
}

// gpio_unexport
// Call to unexport gpio address

int gpio_unexport(GPIO_ADDRESS address)
{
	char sAddress[IO_MAXBUFFER+1];
	snprintf(sAddress,IO_MAXBUFFER,"%d",address);

	return io_write(GPIO_SYSFS "/unexport",sAddress);
}

// gpio_get_direction
// Call to get gpio address direction

int gpio_get_direction(GPIO_ADDRESS address,GPIO_DIRECTION *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/direction",address);

	return io_decode(sDevice,g_sDirection,(int *) value);
}

// gpio_set_direction
// Call to set gpio address direction

int gpio_set_direction(GPIO_ADDRESS address,GPIO_DIRECTION value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/direction",address);

	return io_write(sDevice,g_sDirection[(int) value]);
}

// gpio_get_edge
// Call to get gpio address edge

int gpio_get_edge(GPIO_ADDRESS address,GPIO_EDGE *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/edge",address);

	return io_decode(sDevice,g_sEdge,(int *) value);
}

// gpio_set_edge
// Call to set gpio address edge

int gpio_set_edge(GPIO_ADDRESS address,GPIO_EDGE value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/edge",address);

	return io_write(sDevice,g_sEdge[(int) value]);
}

// gpio_get_activelow
// Call to get gpio address active low

int gpio_get_activelow(GPIO_ADDRESS address,GPIO_ACTIVELOW *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/active_low",address);

	return io_decode(sDevice,g_sBoolean,(int *) value);
}

// gpio_set_activelow
// Call to set gpio address active low

int gpio_set_activelow(GPIO_ADDRESS address,GPIO_ACTIVELOW value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/active_low",address);

	return io_write(sDevice,g_sBoolean[(int) value]);
}

// gpio_get_value
// Call to get gpio address value

int gpio_get_value(GPIO_ADDRESS address,GPIO_VALUE *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/value",address);

	return io_decode(sDevice,g_sBoolean,(int *) value);
}

// gpio_set_value
// Call to set gpio address value

int gpio_set_value(GPIO_ADDRESS address,GPIO_VALUE value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,GPIO_SYSFS "/gpio%d/value",address);

	return io_write(sDevice,g_sBoolean[(value==0)?0:1]);
}
