// channel.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "engine.h"
#include "table.h"
#include "value.h"
#include "channel.h"
#include "anio.h"
#include "gpio.h"
#include "pwmio.h"
#include "i2cio.h"

// Defines

#define PWM_DUTY_CYCLE_1V  (100000)

// Local functions

static int channel_find(CHANNEL *channels,ENGINE_CHANNEL channel);
static int channel_alloc(CHANNEL *channels);

//static int virtual_channel_find(VIRTUAL_CHANNEL *virtualChannels,ENGINE_CHANNEL channel);
//static int virtual_channel_alloc(VIRTUAL_CHANNEL *virtualChannels);

static int channel_open(CHANNEL_ITEM *item);
static int channel_close(CHANNEL_ITEM *item);

static int channel_analog_open(CHANNEL_ITEM *item);
static int channel_analog_close(CHANNEL_ITEM *item);
static int channel_analog_read(CHANNEL_ITEM *item,ENGINE_VALUE *value);
static int channel_analog_write(CHANNEL_ITEM *item,ENGINE_VALUE *value);

static int channel_digital_open(CHANNEL_ITEM *item);
static int channel_digital_close(CHANNEL_ITEM *item);
static int channel_digital_read(CHANNEL_ITEM *item,ENGINE_VALUE *value);
static int channel_digital_write(CHANNEL_ITEM *item,ENGINE_VALUE *value);

static int channel_pwm_open(CHANNEL_ITEM *item);
static int channel_pwm_close(CHANNEL_ITEM *item);
static int channel_pwm_read(CHANNEL_ITEM *item,ENGINE_VALUE *value);
static int channel_pwm_write(CHANNEL_ITEM *item,ENGINE_VALUE *value);

static int channel_i2c_open(CHANNEL_ITEM *item);
static int channel_i2c_close(CHANNEL_ITEM *item);
static int channel_i2c_read(CHANNEL_ITEM *item,ENGINE_VALUE *value);
static int channel_i2c_write(CHANNEL_ITEM *item,ENGINE_VALUE *value);

static int channel_virtual_read(CHANNEL_ITEM *item,ENGINE_VALUE *value);
static int channel_virtual_write(CHANNEL_ITEM *item,ENGINE_VALUE *value);

// Public data

char *g_sChannelType[]={"analog","digital","pwm","triac","i2c","virtA","virtD",NULL};
char *g_sChannelDirection[]={"in","out","high","low",NULL};

// Public functions

// channel_init
// Call to initialize channel list

int channel_init(CHANNEL *channels,int nItems)
{
	int nSize=nItems*sizeof(CHANNEL_ITEM);

	channels->nItems=nItems;
	channels->nCount=0;

	channels->items=(CHANNEL_ITEM *) malloc(nSize);
	memset(channels->items,0,nSize);

	return 0;
}

//int virtual_channel_init(VIRTUAL_CHANNEL *virtualChannels,int nItems)
//{
//	int nSize=nItems*sizeof(VIRTUAL_CHANNEL_ITEM);
//
//	virtualChannels->nItems=nItems;
//	virtualChannels->nCount=0;
//
//	virtualChannels->items=(VIRTUAL_CHANNEL_ITEM *) malloc(nSize);
//	memset(virtualChannels->items,0,nSize);
//
//	return 0;
//}

// channel_exit
// Call to release channel list

int channel_exit(CHANNEL *channels)
{
	int n;

	// close all channels
	for(n=0;n<channels->nItems;n++)
	{
		CHANNEL_ITEM *item=&channels->items[n];
		channel_close(item);
	}

	// free items?
	if(channels->items!=NULL)
	{
		free(channels->items);
		channels->items=NULL;
	}

	channels->nItems=0;
	channels->nCount=0;

	return 0;
}

// virtual_channel_exit
// Call to release channel list

//int virtual_channel_exit(VIRTUAL_CHANNEL *virtualChannels)
//{
//	// free items?
//	if(virtualChannels->items!=NULL)
//	{
//		free(virtualChannels->items);
//		virtualChannels->items=NULL;
//	}
//
//	virtualChannels->nItems=0;
//	virtualChannels->nCount=0;
//
//	return 0;
//}

// channel_add
// Call to add item to channel list

int channel_add(CHANNEL *channels,ENGINE_CHANNEL channel,CHANNEL_ENABLE enable,CHANNEL_TYPE type,CHANNEL_DIRECTION direction,CHANNEL_DEVICE device,CHANNEL_ADDRESS address,int nTrigger,VALUE_CONV *conv)
{
	// already added?
	int n=channel_find(channels,channel);
	if(n>=0) return 0;

	// get next free
	n=channel_alloc(channels);

	if(n<0)
	{
		engine_error("out of channel space");
		return -1;
	}

	// enable poll item
	CHANNEL_ITEM *item=&channels->items[n];

	item->nUsed=1;

	item->nTrigger=nTrigger;
	item->nFailed=0;
	item->nExport=0;

	item->channel=channel;

	item->enable=enable;
	item->type=type;
	item->direction=direction;
	item->device=device;
	item->address=address;

	item->conv=*conv;

	// add to count
	channels->nCount++;

	return 0;
}

// virtual_channel_add
// Call to add item to virtual channel list

//int virtual_channel_add(VIRTUAL_CHANNEL *virtualChannels,ENGINE_CHANNEL channel,CHANNEL_ENABLE enable,VIRTUAL_CHANNEL_TYPE type)
//{
//	// already added?
//	int n=virtual_channel_find(virtualChannels,channel);
//	if(n>=0) return 0;
//
//	// get next free
//	n=virtual_channel_alloc(virtualChannels);
//
//	if(n<0)
//	{
//		engine_error("out of channel space");
//		return -1;
//	}
//
//	// enable poll item
//	VIRTUAL_CHANNEL_ITEM *item=&virtualChannels->items[n];
//
//	item->nUsed=1;
//	item->nFailed=0;
//	item->channel=channel;
//	item->enable=enable;
//	item->type=type;
//
//	// add to count
//	virtualChannels->nCount++;
//	//printf("engine: success: Channel %d added\n", channel);
//	return 0;
//}

// channel_remove
// Call to remove channel item

int channel_remove(CHANNEL *channels,ENGINE_CHANNEL channel)
{
	// find channel?
	int n=channel_find(channels,channel);

	if(n>=0)
	{
		// close the channel
		CHANNEL_ITEM *item=&channels->items[n];
		channel_close(item);

		// mark as unused
		item->nUsed=0;
		channels->nCount--;

		return 0;
	}
	return -1;
}

// channel_read
// Call to read value from channel

int channel_read(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value,TABLE *tables)
{
	// find channel?
	int n=channel_find(channels,channel);

	if(n>=0)
	{
		CHANNEL_ITEM *item=&channels->items[n];

		// channel is disabled?
		if(item->enable==CHANNEL_ENABLE_DISABLED)
		{
			value_status(value,ENGINE_STATUS_DISABLED);
			return 0;
		}

		// must be an input
//		if(item->direction==CHANNEL_DIRECTION_IN)
		{
			int err=-1;

			// open channel?
			if(channel_open(item)>=0)
			{
				// what type?
				switch(item->type)
				{
					case CHANNEL_TYPE_ANALOG:
						// read analog
						err=channel_analog_read(item,value);
						break;

					case CHANNEL_TYPE_DIGITAL:
						// read digital
						err=channel_digital_read(item,value);
						break;

					case CHANNEL_TYPE_PWM:
						// read pwm
						err=channel_pwm_read(item,value);
						break;

					case CHANNEL_TYPE_TRIAC:
						// read triac
						err=channel_digital_read(item,value);
						break;

					case CHANNEL_TYPE_I2C:
						// read I2C
						err=channel_i2c_read(item,value);
						break;
						
					case CHANNEL_TYPE_VIRTUAL_ANALOG:
					case CHANNEL_TYPE_VIRTUAL_DIGITAL:
						err=channel_virtual_read(item,value);
						break;
				}
			}

			// error status?
			if(err<0)
			{
				value_status(value,ENGINE_STATUS_DOWN);
				return 0;
			}

			// No conversion for virtuals
			if((item->type==CHANNEL_TYPE_VIRTUAL_ANALOG) || 
					(item->type==CHANNEL_TYPE_VIRTUAL_DIGITAL))
			{
				return 0;
			}
			
			// convert from raw
			value_convert(value,&item->conv,tables);

			// set trigger on transition to low?
			if(value->cur==0.0)
				value->trigger=item->nTrigger;

			return 0;
		}
	}

	// set to fault
	value_status(value,ENGINE_STATUS_FAULT);

	return -1;
}

// channel_write
// Call to write value to channel

int channel_write(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value,TABLE *tables)
{
	// find channel?
	int n=channel_find(channels,channel);
	//printf("<<engine>> n:%d channel:%d\n", n, channel);

	if(n>=0)
	{
		CHANNEL_ITEM *item=&channels->items[n];

		// must be an output
		if(item->direction==CHANNEL_DIRECTION_OUT && value->flags!=0)
		{
			//printf("<<engine>> Direction Out\n");
			// channel is disabled?
			if(item->enable==CHANNEL_ENABLE_DISABLED)
				return value_status(value,ENGINE_STATUS_DISABLED);

			// convert data
			if (item->type!=CHANNEL_TYPE_VIRTUAL_ANALOG && item->type!=CHANNEL_TYPE_VIRTUAL_DIGITAL)
			{
				if(value->flags==ENGINE_DATA_RAW) value_convert(value,&item->conv,tables);
				if(value->flags==ENGINE_DATA_CUR) value_revert(value,&item->conv,tables);
			}
			
			int err=-1;

			// open channel?
			if(channel_open(item)>=0)
			{
				//printf("<<engine>> channel opened\n");
				// what type?
				switch(item->type)
				{
					case CHANNEL_TYPE_ANALOG:
						// write analog
						err=channel_analog_write(item,value);
						break;

					case CHANNEL_TYPE_DIGITAL:
						// write digital
						err=channel_digital_write(item,value);
						break;

					case CHANNEL_TYPE_PWM:
						// pwm write
						err=channel_pwm_write(item,value);
						break;

					case CHANNEL_TYPE_TRIAC:
						// write triac
						err=channel_digital_write(item,value);
						break;

					case CHANNEL_TYPE_I2C:
						// write i2c
						err=channel_i2c_write(item,value);
						break;
						
					case CHANNEL_TYPE_VIRTUAL_ANALOG:
					case CHANNEL_TYPE_VIRTUAL_DIGITAL:
						//printf("<<engine>> channel:%d\n",channel);
						err=channel_virtual_write(item,value);
						break;
				}
			}

			// error status?
			if(err<0) value_status(value,ENGINE_STATUS_DOWN);

			return 0;
		}
	}

	// set to fault
	value_status(value,ENGINE_STATUS_FAULT);

	return -1;
}

//int virtual_channel_write(VIRTUAL_CHANNEL *virtualChannels,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
//{
//	int err=-1;
//	
//	// find channel?
//	int n=virtual_channel_find(virtualChannels,channel);
//
//	if(n>=0)
//	{
//		VIRTUAL_CHANNEL_ITEM *item=&virtualChannels->items[n];
//
//		// channel is disabled?
//		if(item->enable==CHANNEL_ENABLE_DISABLED)
//		{
//			item->value.status=ENGINE_STATUS_DISABLED;
//			return 0;
//		}
//
//		if (value->flags!=0)
//		{
//			item->value.data = (value->flags==ENGINE_DATA_CUR)?(value->cur):(value->raw);
//			err=0;
//		}
//		
//		// error status?
//		if(err<0)
//		{
//			item->value.status=ENGINE_STATUS_DOWN;
//		}
//
//		return 0;
//	}
//
//	// set to fault
//	printf("engine: error: Channel %d not found\n", channel);
//	//item->value.status=ENGINE_STATUS_FAULT;

//	return -1;
//}

// channel_convert
// Call to convert value using channel settings

int channel_convert(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value,TABLE *tables)
{
	// find channel?
	int n=channel_find(channels,channel);

	if(n>=0)
	{
		CHANNEL_ITEM *item=&channels->items[n];

		// convert data
		if(value->flags==ENGINE_DATA_RAW) value_convert(value,&item->conv,tables);
		if(value->flags==ENGINE_DATA_CUR) value_revert(value,&item->conv,tables);

		return 0;
	}

	// set to fault
	value_status(value,ENGINE_STATUS_FAULT);

	return -1;
}

// channel_report
// Call to display list of channels

int channel_report(CHANNEL *channels)
{
	// output header
	printf("\nchannel type    dir     device  address table   trigger failed  export\n");
	printf("------- ------- ------- ------- ------- ------- ------- ------- -------\n");

	// output channel items
	int n;

	for(n=0;n<channels->nItems;n++)
	{
		CHANNEL_ITEM *item=&channels->items[n];

		// item in use?
		if(item->nUsed!=0)
		{
			char sTrigger[17];
			char sTable[17];

			snprintf(sTrigger,16,"%d",item->nTrigger);
			snprintf(sTable,16,"%d",item->conv.nTable);

			printf("%-7d %-7.7s %-7.7s %-7d %-7d %-7.7s %-7.7s %-7.7s %s\n",
				item->channel,
				g_sChannelType[(int) item->type],
				g_sChannelDirection[(int) item->direction],
				item->device,
				item->address,
				(item->conv.nTable>=0)?sTable:"n/a",
				(item->nTrigger>0)?sTrigger:"off",
				(item->nFailed==0)?"no":"yes",
				(item->nExport==0)?"no":"yes");
		}
	}
	printf("\n");

	return 0;
}

// virtual_channel_report
// Call to display list of virtual channels

//int virtual_channel_report(VIRTUAL_CHANNEL *virtualChannels)
//{
//	// output header
//	printf("\nchannel type            value   failed\n");
//	printf("------- --------------- ------- -------\n");
//
//	// output channel items
//	int n;
//
//	for(n=0;n<virtualChannels->nItems;n++)
//	{
//		VIRTUAL_CHANNEL_ITEM *item=&virtualChannels->items[n];
//		
//		// item in use?
//		if(item->nUsed!=0)
//		{
//			if (item->type==VIRTUAL_CHANNEL_TYPE_DIGITAL)
//			{
//				printf("%-7d %-15.15s %-7.7s %-7.7s\n",
//					item->channel,
//					g_sChannelType[(int) item->type],
//					((int)item->value.data==0)?"Off":"On",
//					(item->nFailed==0)?"no":"yes");
//			}
//			else
//			{
//				printf("%-7d %-15.15s %-3.3lf %-7.7s\n",
//					item->channel,
//					g_sChannelType[(int) item->type],
//					item->value.data,
//					(item->nFailed==0)?"no":"yes");	
//			}
//			
//		}
//	}
//	printf("\n");
//
//	return 0;
//}

// Local functions

// channel_find
// Call to find existing channel item

static int channel_find(CHANNEL *channels,ENGINE_CHANNEL channel)
{
	int n;

	for(n=0;n<channels->nItems;n++)
	{
		// item found?
		CHANNEL_ITEM *item=&channels->items[n];

		if(item->nUsed!=0 && item->channel==channel)
			return n;
	}
	return -1;
}

// virtual_channel_find
// Call to find existing channel item

//static int virtual_channel_find(VIRTUAL_CHANNEL *virtualChannels,ENGINE_CHANNEL channel)
//{
//	int n;
//
//	for(n=0;n<virtualChannels->nItems;n++)
//	{
//		// item found?
//		VIRTUAL_CHANNEL_ITEM *item=&virtualChannels->items[n];
//
//		if(item->nUsed!=0 && item->channel==channel)
//			return n;
//	}
//	return -1;
//}

// channel_alloc
// Call to find first free channel item

static int channel_alloc(CHANNEL *channels)
{
	int n;

	for(n=0;n<channels->nItems;n++)
	{
		// item not in use?
		if(channels->items[n].nUsed==0)
			return n;
	}
	return -1;
}

// virtual_channel_alloc
// Call to find first free virtual channel item

//static int virtual_channel_alloc(VIRTUAL_CHANNEL *virtualChannels)
//{
//	int n;
//
//	for(n=0;n<virtualChannels->nItems;n++)
//	{
//		// item not in use?
//		if(virtualChannels->items[n].nUsed==0)
//			return n;
//	}
//	return -1;
//}

// channel_open
// Call to open channel hardware

static int channel_open(CHANNEL_ITEM *item)
{
	// has failed?
	if(item->nFailed==1) return -1;

	int err=-1;

	// what type?
	switch(item->type)
	{
		case CHANNEL_TYPE_ANALOG:
			// analog channel
			err=channel_analog_open(item);
			break;

		case CHANNEL_TYPE_DIGITAL:
			// digital channel
			err=channel_digital_open(item);
			break;

		case CHANNEL_TYPE_PWM:
			// pwm channel
			err=channel_pwm_open(item);
			break;

		case CHANNEL_TYPE_TRIAC:
			// triac channel
			err=channel_digital_open(item);
			break;

		case CHANNEL_TYPE_I2C:
			// I2C channel
			err=channel_i2c_open(item);
			break;
			
		case CHANNEL_TYPE_VIRTUAL_ANALOG:
		case CHANNEL_TYPE_VIRTUAL_DIGITAL:
			err=0;
			break;
	}

	// fail on error
	if(err<0) item->nFailed=1;

	return 0;
}

// channel_close
// Call to close channel hardware

static int channel_close(CHANNEL_ITEM *item)
{
	// was open?
	if(item->nFailed==1 || item->nExport==0) return 0;

	int err=-1;

	// close channel
	switch(item->type)
	{
		case CHANNEL_TYPE_ANALOG:
			// analog channel
			err=channel_analog_close(item);
			break;

		case CHANNEL_TYPE_DIGITAL:
			// digital channel
			err=channel_digital_close(item);
			break;

		case CHANNEL_TYPE_PWM:
			// pwm channel
			err=channel_pwm_close(item);
			break;

		case CHANNEL_TYPE_TRIAC:
			// triac channel
			err=channel_digital_close(item);
			break;

		case CHANNEL_TYPE_I2C:
			// I2C channel
			err=channel_i2c_close(item);
			break;
			
		case CHANNEL_TYPE_VIRTUAL_ANALOG:
	    case CHANNEL_TYPE_VIRTUAL_DIGITAL:
			err=0;
			break;
	}
	return err;
}

// channel_analog_open
// Call to open analog channel

static int channel_analog_open(CHANNEL_ITEM *item)
{
	return 0;
}

// channel_analog_close
// Call to close analog channel

static int channel_analog_close(CHANNEL_ITEM *item)
{
	return 0;
}

// channel_analog_read
// Call to read analog value

static int channel_analog_read(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	// read current value?
	ANIO_DEVICE device=(ANIO_DEVICE) item->device;
	ANIO_ADDRESS address=(ANIO_ADDRESS) item->address;

	ANIO_VALUE raw;

	int err=anio_get_value(device,address,&raw);
	if(err>=0) value_raw(value,(ENGINE_DATA) raw);
	
	return err;
}

// channel_analog_write
// Call to write analog value

static int channel_analog_write(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	return -1;
}

// channel_digital_open
// Call to open digital channel

static int channel_digital_open(CHANNEL_ITEM *item)
{
	GPIO_ADDRESS address=(GPIO_ADDRESS) item->address;
	GPIO_DIRECTION direction=(GPIO_DIRECTION) item->direction;

	// already exported?
	if(gpio_exists(address)<0)
	{
		// export gpio address?
		if(gpio_export(address)<0)
			return -1;

		// set direction
		gpio_set_direction(address,direction);

		// remove high and low
		if(direction!=GP_IN)
			item->direction=CHANNEL_DIRECTION_OUT;

		item->nExport=1;
	}
	else
	{
		gpio_get_direction(address, &direction);
		if (direction != (GPIO_DIRECTION)item->direction)
		{
			gpio_set_direction(address, (GPIO_DIRECTION)item->direction);

			// remove high and low
			if((GPIO_DIRECTION)item->direction!=GP_IN)
				item->direction=CHANNEL_DIRECTION_OUT;
		}
	}
	return 0;
}

// channel_digital_close
// Call to close digital channel

static int channel_digital_close(CHANNEL_ITEM *item)
{
	GPIO_ADDRESS address=(GPIO_ADDRESS) item->address;

	// unexport gpio address?
	if(gpio_unexport(address)>=0)
	{
		item->nExport=0;
		return 0;
	}
	return -1;
}

// channel_digital_read
// Call to read digital value

static int channel_digital_read(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	GPIO_ADDRESS address=(GPIO_ADDRESS) item->address;
	GPIO_VALUE raw;

	// read current value?
	int err=gpio_get_value(address,&raw);
	if(err>=0) value_raw(value,(ENGINE_DATA) raw);

	return err;
}

// channel_digital_write
// Call to write digital value

static int channel_digital_write(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	GPIO_ADDRESS address=(GPIO_ADDRESS) item->address;
	GPIO_VALUE raw=(GPIO_VALUE) value->raw;
	
	// write current value?
	return gpio_set_value(address,raw);
}

// channel_pwm_open
// Call to open pwm channel

static int channel_pwm_open(CHANNEL_ITEM *item)
{
	PWMIO_CHIP chip=(PWMIO_CHIP) item->device;
	PWMIO_CHANNEL channel=(PWMIO_CHANNEL) item->address;

	// already exported?
	if(pwmio_exists(chip,channel)<0)
	{
		// export pwm channel?
		if(pwmio_export(chip,channel)<0)
			return -1;

		// set polarity and period
		pwmio_set_polarity(chip,channel,PWMIO_POLARITY_NORMAL);
		//pwmio_set_period(chip,channel,item->conv.high);
		pwmio_set_period(chip,channel,(item->conv.high * PWM_DUTY_CYCLE_1V));

		// enable output
		pwmio_set_enable(chip,channel,PWMIO_ENABLE_ENABLED);

		item->nExport=1;
	}
	return 0;
}

// channel_pwm_close
// Call to close pwm channel

static int channel_pwm_close(CHANNEL_ITEM *item)
{
	PWMIO_CHIP chip=(PWMIO_CHIP) item->device;
	PWMIO_CHANNEL channel=(PWMIO_CHANNEL) item->address;

	// disable output
	pwmio_set_enable(chip,channel,PWMIO_ENABLE_DISABLED);

	// unexport pwm channel?
	if(pwmio_unexport(chip,channel)>=0)
	{
		item->nExport=0;
		return 0;
	}
	return -1;
}

// channel_pwm_read
// Call to read pwm value

static int channel_pwm_read(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	PWMIO_CHIP chip=(PWMIO_CHIP) item->device;
	PWMIO_CHANNEL channel=(PWMIO_CHANNEL) item->address;

	PWMIO_DUTY raw;
	double raw_temp;

	// read current value?
	int err=pwmio_get_duty(chip,channel,&raw);
	raw_temp = (double)raw/PWM_DUTY_CYCLE_1V;
	//if(err>=0) value_raw(value,(ENGINE_DATA) raw);
	if(err>=0) value_raw(value,(ENGINE_DATA) raw_temp);

	return err;
}

// channel_pwm_write
// Call to write pwm value

static int channel_pwm_write(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	PWMIO_CHIP chip=(PWMIO_CHIP) item->device;
	PWMIO_CHANNEL channel=(PWMIO_CHANNEL) item->address;

	//PWMIO_DUTY raw=(PWMIO_DUTY) value->raw;
	PWMIO_DUTY raw=(PWMIO_DUTY) (value->raw * PWM_DUTY_CYCLE_1V);

	// write current value?
	return pwmio_set_duty(chip,channel,raw);
}

// channel_i2c_open
// Call to open i2c channel

static int channel_i2c_open(CHANNEL_ITEM *item)
{
	// i2c found?
	I2CIO_DEVICE device=(I2CIO_DEVICE) item->device;
	I2CIO_ADDRESS address=(I2CIO_ADDRESS) item->address;

	return i2cio_exists(device,address);
}

// channel_i2c_close
// Call to close i2c channel

static int channel_i2c_close(CHANNEL_ITEM *item)
{
	return 0;
}

// channel_i2c_read
// Call to read i2c value

static int channel_i2c_read(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	// read measurement?
	I2CIO_DEVICE device=(I2CIO_DEVICE) item->device;
	I2CIO_ADDRESS address=(I2CIO_ADDRESS) item->address;

	I2CIO_VALUE raw;

	int err=i2cio_get_measurement(device,address,&raw);
	if(err>=0) value_raw(value,(ENGINE_DATA) raw);

	return err;
}

// channel_i2c_write
// Call to write i2c value

static int channel_i2c_write(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	return -1;
}

//typedef struct {
//	ENGINE_CHANNEL channel;
//	ENGINE_VALUE value;
//} VIRTUAL_CHANNEL_DATA_ITEM;

//typedef struct {
//	int nItems;
//	VIRTUAL_CHANNEL_DATA_ITEM * items;
//}VIRTUAL_CHANNEL_DATA;
	
static int channel_virtual_read(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	//TODO: Check value's status
	value->cur=item->value.cur;
	value->status=ENGINE_STATUS_OK;
	
	return 0;
}

static int channel_virtual_write(CHANNEL_ITEM *item,ENGINE_VALUE *value)
{
	//printf("<<engine>> channel_virtual_write\n");
	item->value.cur=value->cur;
	item->value.status=ENGINE_STATUS_OK;
	value->status=ENGINE_STATUS_OK;
	//printf("<<engine>> cur:%f  status:%d\n", item->value.cur, item->value.status);
	return 0;
}
