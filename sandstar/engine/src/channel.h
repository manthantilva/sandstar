// channel.h

#ifndef __CHANNEL_H__
#define __CHANNEL_H__

// Definitions

#define MAX_CHANNELS	      64
//#define MAX_VIRTUAL_CHANNELS  10

// Channel enable

enum _CHANNEL_ENABLE
{
	CHANNEL_ENABLE_DISABLED,
	CHANNEL_ENABLE_ENABLED
};

typedef enum _CHANNEL_ENABLE CHANNEL_ENABLE;

// Channel type

enum _CHANNEL_TYPE
{
	CHANNEL_TYPE_ANALOG,
	CHANNEL_TYPE_DIGITAL,
	CHANNEL_TYPE_PWM,
	CHANNEL_TYPE_TRIAC,
	CHANNEL_TYPE_I2C,
	CHANNEL_TYPE_VIRTUAL_ANALOG,
	CHANNEL_TYPE_VIRTUAL_DIGITAL
};

typedef enum _CHANNEL_TYPE CHANNEL_TYPE;

//enum _VIRTUAL_CHANNEL_TYPE
//{
//	VIRTUAL_CHANNEL_TYPE_ANALOG,
//	VIRTUAL_CHANNEL_TYPE_DIGITAL,
//	VIRTUAL_CHANNEL_TYPE_NONE
//};

//typedef enum _VIRTUAL_CHANNEL_TYPE VIRTUAL_CHANNEL_TYPE;

// Channel direction

enum _CHANNEL_DIRECTION
{
	CHANNEL_DIRECTION_IN,
	CHANNEL_DIRECTION_OUT,
	CHANNEL_DIRECTION_HIGH,
	CHANNEL_DIRECTION_LOW,
	CHANNEL_DIRECTION_NONE
};

typedef enum _CHANNEL_DIRECTION CHANNEL_DIRECTION;

// Typedefs

typedef unsigned int CHANNEL_DEVICE;
typedef unsigned int CHANNEL_ADDRESS;

// Channel item

struct _CHANNEL_ITEM
{
	int nUsed;

	int nTrigger;
	int nFailed;
	int nExport;

	ENGINE_CHANNEL channel;

	CHANNEL_ENABLE enable;
	CHANNEL_TYPE type;
	CHANNEL_DIRECTION direction;
	CHANNEL_DEVICE device;
	CHANNEL_ADDRESS address;

	VALUE_CONV conv;
	
	ENGINE_VALUE value;
};

typedef struct _CHANNEL_ITEM CHANNEL_ITEM;

// Channel struct

struct _CHANNEL
{
	int nItems;
	int nCount;

	CHANNEL_ITEM *items;
};

typedef struct _CHANNEL CHANNEL;

// Virtual Channel item

//struct _VIRTUAL_CHANNEL_ITEM
//{
//	int nUsed;
//	int nFailed;
//
//	ENGINE_CHANNEL channel;
//
//	CHANNEL_ENABLE enable;
//	VIRTUAL_CHANNEL_TYPE type;
//	
//	VIRTUAL_CHANNEL_VALUE value;
//};

//typedef struct _VIRTUAL_CHANNEL_ITEM VIRTUAL_CHANNEL_ITEM;

// Channel struct

//struct _VIRTUAL_CHANNEL
//{
//	int nItems;
//	int nCount;
//
//	VIRTUAL_CHANNEL_ITEM *items;
//};

//typedef struct _VIRTUAL_CHANNEL VIRTUAL_CHANNEL;

// Global data

extern char *g_sChannelType[];
extern char *g_sChannelDirection[];

// Public functions

int channel_init(CHANNEL *channels,int nItems);
int channel_exit(CHANNEL *channels);

//int virtual_channel_init(VIRTUAL_CHANNEL *virtualChannels,int nItems);
//int virtual_channel_exit(VIRTUAL_CHANNEL *virtualChannels);

int channel_add(CHANNEL *channels,ENGINE_CHANNEL channel,CHANNEL_ENABLE enable,CHANNEL_TYPE type,CHANNEL_DIRECTION direction,CHANNEL_DEVICE device,CHANNEL_ADDRESS address,int nTrigger,VALUE_CONV *conv);
int channel_remove(CHANNEL *channels,ENGINE_CHANNEL channel);

//int virtual_channel_add(VIRTUAL_CHANNEL *virtualChannels,ENGINE_CHANNEL channel,CHANNEL_ENABLE enable,VIRTUAL_CHANNEL_TYPE type);

int channel_read(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value,TABLE *tables);
int channel_write(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value,TABLE *tables);

//int virtual_channel_write(VIRTUAL_CHANNEL *virtualChannels,ENGINE_CHANNEL channel,ENGINE_VALUE *value);

int channel_convert(CHANNEL *channels,ENGINE_CHANNEL channel,ENGINE_VALUE *value,TABLE *tables);

int channel_report(CHANNEL *channels);

//int virtual_channel_report(VIRTUAL_CHANNEL *virtualChannels);

#endif // __CHANNEL_H__
