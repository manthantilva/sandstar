// io.c

// Include files

#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "global.h"
#include "engine.h"
#include "io.h"

// io_exists
// Call to check if device exists

int io_exists(char *sDevice)
{
	struct stat buffer;

	if(stat(sDevice,&buffer)<0)
	{
		engine_error("io failed to detect %s",sDevice);
		return -1;
	}
	return 0;
}

// io_open
// Call to open device

int io_open(char *sDevice,int nOpen)
{
	// can open it?
	int hDevice=open(sDevice,nOpen);

	if(hDevice<0)
	{
		engine_error("io failed to open %s",sDevice);
		return -1;
	}
	return hDevice;
}

// io_close
// Call to close device

int io_close(int hDevice)
{
	// can close it?
	if(close(hDevice)<0)
	{
		engine_error("io failed to close device");
		return -1;
	}
	return 0;
}

// io_read
// Call to read from device

int io_read(char *sDevice,char *sBuffer)
{
	// can open it?
	int hDevice=io_open(sDevice,O_RDONLY);
	if(hDevice<0) return -1;

	// read buffer?
	int nSize=read(hDevice,sBuffer,IO_MAXBUFFER);
	io_close(hDevice);

	if(nSize<0)
	{
		engine_error("io failed to read from %s",sDevice);
		return -1;
	}

	// mark end of buffer
	int n;

	for(n=0;n<nSize;n++)
		if(sBuffer[n]<' ') sBuffer[n]=0;

	sBuffer[nSize]=0;

	return 0;
}

// io_write
// Call to write to device

int io_write(char *sDevice,char *sBuffer)
{
	// can open it?
	int hDevice=io_open(sDevice,O_WRONLY);
	if(hDevice<0) return -1;

	// buffer length
	int nLength=strlen(sBuffer);

	// write buffer?
	int nSize=write(hDevice,sBuffer,nLength);
	io_close(hDevice);

	if(nSize!=nLength)
	{
		engine_error("io failed to write to %s",sDevice);
		return -1;
	}
	return 0;
}

// io_decode
// Call to read and decode response

int io_decode(char *sDevice,char *sResponses[],int *value)
{
	char sBuffer[IO_MAXBUFFER+1];

	// read buffer?
	if(io_read(sDevice,sBuffer)>=0)
	{
		// find response from list
		int n;

		for(n=0;sResponses[n]!=NULL;n++)
		{
			// found it?
			if(strcmp(sBuffer,sResponses[n])==0)
			{
				*value=n;
				return 0;
			}
		}

		// response not found
		engine_error("io failed to decode response %s from %s",sBuffer,sDevice);
	}
	return -1;
}

// io_ext
// Call to get file extension of device

const char *io_ext(const char *sDevice)
{
	const char *dot=strrchr(sDevice,'.');
	if(dot==NULL || dot==sDevice) return "";

	return dot+1;
}
